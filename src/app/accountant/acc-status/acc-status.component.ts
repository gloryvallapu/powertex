import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { GlobalServiceService } from '../../global-service.service';
import { DataServiceService } from '../../data-service.service';
import { Options } from 'ng5-slider';
// import { ComponentCommunicationService } from '../component-communication.service';
import { NgxSmartModalService } from 'ngx-smart-modal';
declare var $: any;

@Component({
  selector: 'app-acc-status',
  templateUrl: './acc-status.component.html',
  styleUrls: ['./acc-status.component.scss']
})
export class AccStatusComponent implements OnInit {
  POhidden:boolean=true;
  DOhidden:boolean=false;
  GRNhidden:boolean=false;
  PENhidden:boolean;
  sub: any;
  page: any = 1;
  orders: any;
  loginUserData: any;
  token: any;
  alert: boolean;
  obj: any = {};
  user_id: any;
  icon: boolean;
  panelOpenState = false;
  inv_data: any;
  constructor(private router: Router, private route: ActivatedRoute, private service: GlobalServiceService, public dataService: DataServiceService,
     private ngxSmartService: NgxSmartModalService) {
    this.obj.id = 4;
  }

  ngOnInit() {
    this.alert = false;
    this.token = localStorage.getItem('token');
    this.loginUserData = JSON.parse(localStorage.getItem('loginUserData'));
    if(this.loginUserData===null){
      this.router.navigateByUrl('home');
    }
    this.sub = this.route.params.subscribe(params => {
      console.log(this.loginUserData);
      
      if (this.token == null) {
        this.user_id = '';
      } else {
        this.user_id = this.loginUserData.user_id;
      }
      

    });
    this.getOrders();
    this.invoice();

  }

  getOrders(){
    let param1="";
    let param2="";
    let param3="";
    let param4="";

    this.service.getDatawithQueryParams5(7.6, param1, param2, param3,param4,this.loginUserData.company_code).subscribe((resp: Response) => {
      console.log(resp);
      
      this.getorddata(resp);
    });
  }
  invoice(){
    let param1="";
    let param2="";
    this.service.getDatawithQueryParams3(7.6, param1,param2,this.loginUserData.company_code).subscribe((resp: Response) => {
      console.log(resp);
      
      this.getinvdata(resp);
    });
  }
  getorddata(resp){
    console.log(resp.data)
    this.orders=resp.data;
    console.log(this.orders);
  }
  getinvdata(resp){
    this.inv_data=resp.data;
    console.log("inv",this.inv_data);
  }
  po(){
    this.POhidden=true;
  this.DOhidden=false;
  this.GRNhidden=false;
  }
  do(){
    this.POhidden=false;
  this.DOhidden=true;
  this.GRNhidden=false;
  }
  grn(){
    alert("In-Progress")
  }

  // check_status(p) {
  //   console.log("call");
    
  //   for(let pd of p )
  //   {
  //     if(pd.status!=="Delivered") {
  //       return true;
  //     }
  //     return false;
  //   }
  // }
  // check_status1(p){
  //   for(let pd of p )
  //   {
  //     if(pd.status =="Delivered") {
  //       return true;
  //     }
  //     return false;
  //   }
  // }

}
