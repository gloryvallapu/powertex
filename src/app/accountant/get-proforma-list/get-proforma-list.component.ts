import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { GlobalServiceService } from '../../global-service.service';
import { NgxSmartModalService } from 'ngx-smart-modal';
import { NgxSpinnerService } from 'ngx-spinner';

@Component({
  selector: 'app-get-proforma-list',
  templateUrl: './get-proforma-list.component.html',
  styleUrls: ['./get-proforma-list.component.scss']
})
export class GetProformaListComponent implements OnInit {

  proformaList: any = [];
  p: any = 1;
  loginUserData: any;
  searchText: any;

  constructor(private route: Router, private globalService: GlobalServiceService, private ngxSmartService: NgxSmartModalService, private spinner: NgxSpinnerService) { }

  ngOnInit() {
    this.loginUserData = JSON.parse(localStorage.getItem('loginUserData'));
    if(this.loginUserData===null){
      this.route.navigateByUrl('home');
    }
    this.getPOList();
  }
  getPOList() {
    this.spinner.show();
    this.globalService.getDatawithQueryParams1nd4(3.9, 20, this.loginUserData.company_code).subscribe((data) => {
      this.spinner.hide();
      this.proformaList = data;
    },
      error => {
        this.ngxSmartService.getModal('errorModal').open();
        // console.log(error);
      });
  };

  makeInvoice(data) {
    localStorage.setItem('proformaData', JSON.stringify(data));
    this.route.navigateByUrl('invoice');
  }

}
