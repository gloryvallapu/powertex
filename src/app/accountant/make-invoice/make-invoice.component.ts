import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { GlobalServiceService } from '../../global-service.service';
import { formatDate } from "@angular/common";
import { NgxSmartModalService } from 'ngx-smart-modal';
import { NgxSpinnerService } from 'ngx-spinner';
declare var $: any;

@Component({
  selector: 'app-make-invoice',
  templateUrl: './make-invoice.component.html',
  styleUrls: ['./make-invoice.component.scss']
})
export class MakeInvoiceComponent implements OnInit {

  proformaData: any;
  invoiceModel: any = {};
  invoiceListData: any;
  p: any = 1;
  todayDate = new Date();
  headerData: any;
  invoiceList: any = [];
  keys: string[];
  loginUserData: any;
  approveBtn: boolean = false;
  stockDataList: any = [];
  invoice_No: any;
  invoiceData: any;
  billingAddress: any;
  shipingAddress: any;

  constructor(private route: Router, private globalService: GlobalServiceService, private ngxSmartService: NgxSmartModalService, private spinner: NgxSpinnerService) {

  }

  ngOnInit() {
    this.loginUserData = JSON.parse(localStorage.getItem('loginUserData'));
    if(this.loginUserData===null){
      this.route.navigateByUrl('home');
    }
    this.proformaData = JSON.parse(localStorage.getItem('proformaData'));
    console.log(this.proformaData);

    this.headerData = {
      document_no: this.proformaData.seq_no,
      document_date: formatDate(this.todayDate, 'yyyyMMdd', 'en-US'),
      dest_company_code: this.proformaData.ship_to_company_code,
      source_company_code: this.proformaData.inv_origin_company_code,
      inv_type: this.proformaData.inv_type,
      shipment_point: "",
      payment_terms: "",
      currency_code: "",
      exchange_rate: this.proformaData.exchange_rate,
      credit_period: this.proformaData.credit_period,
      spl_instr: "",
      fin_year: this.proformaData.financial_year,
      bill_to_party_seq_no: this.proformaData.bill_to_party_seq_no,
      ship_to_party_seq_no: this.proformaData.ship_to_party_seq_no,
      created_user_id: this.loginUserData.user_id
    }
    this.getpoData();
    this.inv_printAddress();
  };

  getpoData() {
    this.spinner.show();
    this.globalService.getDatawithQueryParams5(3.9, 21, this.proformaData.packing_l_no, this.proformaData.packing_l_date, this.loginUserData.company_code, this.proformaData.financial_year).subscribe((data) => {
      this.spinner.hide();
      this.invoiceListData = data;
      this.invoiceListData.forEach(element => {
        this.invoiceModel[element.po_srl_no] = element.balance_qty;
      });
    },
      error => {
        this.ngxSmartService.getModal('errorModal').open();
        // console.log(error);
      });
  };

  checkQty(qty, packQty) {
    if (packQty > qty) {
      this.approveBtn = true;
    } else {
      this.approveBtn = false;
    }
  }

  makeInvoice() {
    this.spinner.show();
    this.approveBtn = false;
    this.invoiceList = [];
    this.keys = Object.keys(this.invoiceModel);
    for (let i = 0; i < this.invoiceListData.length; i++) {
      for (let j = 0; j < this.keys.length; j++) {
        if (this.invoiceListData[i].po_srl_no == this.keys[j]) {
          if (this.invoiceModel[this.keys[j]] <= this.invoiceListData[i].packing_qty) {
            let json_dtl = {
              "document_no": this.invoiceListData[i].seq_no,
              "document_date": formatDate(this.todayDate, 'yyyyMMdd', 'en-US'),
              "po_no": this.invoiceListData[i].po_no,
              "discount_eff": this.invoiceListData[i].discount_eff,
              "performainv_date": formatDate(this.todayDate, 'yyyyMMdd', 'en-US'),
              "po_srl_no": this.invoiceListData[i].po_srl_no,
              "po_date": (this.invoiceListData[i].po_date).replace(/-/g, ""),
              "productid": this.invoiceListData[i].productid,
              "packing_qty": this.invoiceListData[i].balance_qty,
              "performa_inv_qty": this.invoiceListData[i].performa_inv_qty,
              "performa_inv_no": this.invoiceListData[i].packing_l_no,
              "inv_srl_no": this.invoiceListData[i].inv_srl_no,
              "inv_qty": this.invoiceModel[this.keys[j]],
              "mrp": this.invoiceListData[i].mrp,
              "tot_value": JSON.parse(this.invoiceModel[this.keys[j]]) * JSON.parse(this.invoiceListData[i].net_price),
              "net_price": (JSON.parse(this.invoiceModel[this.keys[j]]) * JSON.parse(this.invoiceListData[i].net_price)) * 1.18,
              "created_user_id": this.loginUserData.user_id,
              "srl_no": this.invoiceListData[i].srl_no
            }
            this.invoiceList.push(json_dtl);
          } else {
            this.invoiceList = [];
            this.approveBtn = true;
            break;
          }
        }
      }
    }

    if (!this.approveBtn) {
      console.log(this.invoiceList);
      this.headerData.credit_period = this.proformaData.credit_period;
      this.headerData.payment_terms = this.proformaData.payment_terms;
      this.headerData.spl_instr = this.proformaData.spl_instr;
      this.headerData.remarks1 = this.proformaData.remarks1;
      this.headerData.remarks2 = this.proformaData.remarks2;
      let body = {
        "process_in": 'INV', "operation_in": "UPDATE", "draft_final_in": "FINAL", "document_no_out": "", "message_out": "",
        "json_hdr": this.headerData, "json_dtl": this.invoiceList
      }
      let methodName = "insert_update/"
      this.globalService.postData(body, methodName).subscribe((data) => {
        this.spinner.hide();
        console.log(data);
        if (data.Message == "Invoice Sucessfully inserted ") {
          this.invoice_No = data.Invoice;
          this.invoiceData = data;
          localStorage.setItem('InvoiceData',JSON.stringify(this.invoiceData));
          console.log("invoice",this.invoiceData);
          $('#makeinvoiceModal').modal('show');
          
        }
      },
        error => {
          this.ngxSmartService.getModal('errorModal').open();
          // console.log(error);
        });

    };
  };

  gotoPrevious() {
    this.route.navigateByUrl('profomaList');
  };

 

  invoice_cnfm(){
    this.route.navigateByUrl('/invoice-Print');
  }

  inv_printAddress() {
    let input_id = "7.8";
    let param1 = this.proformaData.bill_to_party_seq_no + "," + this.proformaData.ship_to_party_seq_no;
    console.log(param1);
    return this.globalService.getDatawithQueryParams1(input_id, param1).subscribe(data => {
      console.log(data);
      this.billingAddress = data.Bill;
      this.shipingAddress = data.SHIP;
      localStorage.setItem('Inv_Address', JSON.stringify(data));
    });
  }
  


}
