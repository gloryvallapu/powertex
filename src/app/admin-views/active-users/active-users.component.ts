import { Component, OnInit, Input } from '@angular/core';
import { Router } from '@angular/router';
import { GlobalServiceService } from '../../global-service.service';
import { DataServiceService } from "../../data-service.service";
import { NgxSpinnerService } from 'ngx-spinner';
import { NgxSmartModalService } from 'ngx-smart-modal';
declare var $: any;

@Component({
  selector: 'app-active-users',
  templateUrl: './active-users.component.html',
  styleUrls: ['./active-users.component.scss']
})
export class ActiveUsersComponent implements OnInit {
  regList: any = [];
  p: any = 1;
  rejData: any;
  rejectData: any = {};
  searchText: any;
  @Input() styleId: number;
  alert: boolean;
  wish_alert: any;
  icon: boolean;

  constructor(private route: Router, private profileService: GlobalServiceService,
    private dataService: DataServiceService, public ngxSmartService: NgxSmartModalService, private spinner: NgxSpinnerService) {
  }

  ngOnInit() {
    this.alert = false;
    this.getRegisterdUsers();
  }

  getRegisterdUsers() {
    this.spinner.show();
    this.profileService.getDatawithQueryParams1(3.5, 'A').subscribe((data) => {
      this.spinner.hide();
      if (data.length > 0)
        this.regList = data;
      else {
        //alert('No data');
      }
    },
      error => {
        this.ngxSmartService.getModal('errorModal').open();
        // console.log(error);
      });
  };

  editInfo(data) {
    this.dataService.saveData(data);
    localStorage.setItem('userProfile', JSON.stringify(data));
    this.route.navigate(['/register-profile-info']);
  };

  reject(data) {
    this.rejData = data;
    $('#confirmModal').modal('show');
  };

  viewInfo(data) {
    this.ngxSmartService.resetModalData('viewmoreModal');
    this.ngxSmartService.setModalData(data, 'viewmoreModal');
    this.ngxSmartService.getModal('viewmoreModal').open();

  };

  confirm() {
    this.spinner.show();
    $('#confirmModal').modal('hide');
    this.rejectData.status = 'R';
    this.rejectData.passing_param = 2;
    this.rejectData.user_type = this.rejData.user_type;
    this.rejectData.user_id = this.rejData.user_id;
    var methodName = 'api/registration/';
    this.profileService.postData(this.rejectData, methodName).subscribe((data) => {
      this.spinner.hide();
      if (data.Status == 'Update sucessfully') {
        //alert(data.Status);
        this.wish_alert = data.Status;
        this.addwish();
        this.icon = true;
        this.getRegisterdUsers();
      } else {
        //alert('Error');
        this.wish_alert = "Error"
        this.addwish();
        this.icon = true;
      }
    },
      error => {
        this.ngxSmartService.getModal('errorModal').open();
        // console.log(error);
      });
  }

  addwish() {
    this.alert = true;
    setInterval(() => {
      this.alert = false;
    }, 5000);
  }


}
