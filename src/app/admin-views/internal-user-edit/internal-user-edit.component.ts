import { Component, OnInit } from '@angular/core';
import { Router } from "@angular/router";
import { DataServiceService } from "../../data-service.service";
import { GlobalServiceService } from "../../global-service.service";
import { NgxSmartModalService } from 'ngx-smart-modal';
import { NgxSpinnerService } from 'ngx-spinner';

@Component({
  selector: 'app-internal-user-edit',
  templateUrl: './internal-user-edit.component.html',
  styleUrls: ['./internal-user-edit.component.scss']
})
export class InternalUserEditComponent implements OnInit {
  profileData: any;
  designationsList: any;
  companiesList: any;
  updateDataModel: any = {};

  constructor(private route: Router, private dataService: DataServiceService, private globalService: GlobalServiceService, private ngxSmartService: NgxSmartModalService, private spinner: NgxSpinnerService) { }

  ngOnInit() {
    this.profileData = JSON.parse(localStorage.getItem('userProfile'));
    this.designationsList = this.dataService.getOnLoadServices(36);
    this.companiesList = this.dataService.getOnLoadServices(3.1);
  };

  resetForm(form) {
    form.reset();
  };

  updateProfile() {
    this.spinner.show();
    this.updateDataModel.status = this.profileData.status;
    this.updateDataModel.user_id = this.profileData.user_id;
    this.updateDataModel.user_type = this.profileData.user_type;
    this.updateDataModel.passing_param = 2;
    var methodName = 'api/registration/';
    this.globalService.postData(this.updateDataModel, methodName).subscribe((data) => {
      this.spinner.hide();
      if (data.Status == 'Update sucessfully') {
        alert(data.Status);
        this.route.navigateByUrl('/internal-users');
      } else {
        alert(data.Status);
      }
    },
      error => {
        this.ngxSmartService.getModal('errorModal').open();
        // console.log(error);
      });
  }


}
