import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { GlobalServiceService } from '../../global-service.service';
import { DataServiceService } from '../../data-service.service';
import { NgxSmartModalService } from 'ngx-smart-modal';
import { NgxSpinnerService } from 'ngx-spinner';

@Component({
  selector: 'app-masters-company',
  templateUrl: './masters-company.component.html',
  styleUrls: ['./masters-company.component.scss']
})
export class MastersCompanyComponent implements OnInit {
  response1: any;
  response: any;
  countriesList: any;
  country: any;
  response2: any;
  state: any;
  response3: any;
  accManagersList: any;
  salesManagersList: any;
  regProfileInfo: any = {};
  ACC: any;
  SM: any;

  constructor(private route: Router, private masterComany: GlobalServiceService, private dataService: DataServiceService, private ngxSmartService: NgxSmartModalService, private spinner: NgxSpinnerService) { }

  ngOnInit() {
    this.countriesList = this.dataService.getOnLoadServices(19);
    this.accManagersList = this.dataService.getOnLoadServices(3.3);
    this.salesManagersList = this.dataService.getOnLoadServices(3.4);
  };

  onSelectCountry(country) {
    this.country = country;
    this.getDatawith1Param('15', country);
  }

  getDatawith1Param(input_id, param) {
    this.spinner.show();
    this.masterComany.getDatawithQueryParams1(input_id, param).subscribe((data) => {
      this.spinner.hide();
      this.response1 = data;
    },
      error => {
        this.ngxSmartService.getModal('errorModal').open();
        // console.log(error);
      });
  };
  onSelectState(state) {
    this.state = state;
    this.getDatawith2Param('16', this.country, state);
  }

  getDatawith2Param(input_id, param1, param2) {
    this.spinner.show();
    this.masterComany.getDatawithQueryParams2(input_id, param1, param2).subscribe((data) => {
      this.spinner.hide();
      this.response2 = data;
    },
      error => {
        this.ngxSmartService.getModal('errorModal').open();
        // console.log(error);
      });
  };
  onSelectDistrict(district) {
    this.getDatawith3Param('17', this.country, this.state, district);
  };

  getDatawith3Param(input_id, param1, param2, param3) {
    this.spinner.show();
    this.masterComany.getDatawithQueryParams3(input_id, param1, param2, param3).subscribe((data) => {
      this.spinner.hide();
      this.response3 = data;
      console.log(data);
    },
      error => {
        this.ngxSmartService.getModal('errorModal').open();
        // console.log(error);
      });
  };

  onSelectAccunt(ACC) {
    this.ACC = ACC;
    console.log(this.ACC);
  }

  onSelectSales(SM) {
    this.SM = SM;
    console.log(this.SM);
  }

  masterCompanySubmit() {
    this.spinner.show();
    let methodName = "api/company/";
    this.masterComany.postData(this.regProfileInfo, methodName).subscribe(data => {
      this.spinner.hide();
      if (data['status'] == "success") {
        //this.regProfileInfo = {};
        alert("success");
      }
    },
      error => {
        this.ngxSmartService.getModal('errorModal').open();
        // console.log(error);
      });
  }


}

