import { Component, OnInit, inject } from '@angular/core';
import { FormControl, Validators, NgForm } from "@angular/forms";
import { GlobalServiceService } from '../../global-service.service';
import { DataServiceService } from '../../data-service.service';
import 'bootstrap';
import { NgxSmartModalService } from 'ngx-smart-modal';
import { NgxSpinnerService } from 'ngx-spinner';
declare var $: any;

@Component({
  selector: 'app-product-pricing',
  templateUrl: './product-pricing.component.html',
  styleUrls: ['./product-pricing.component.scss']
})

export class ProductPricingComponent implements OnInit {

  input_id: any;
  catSubcatList: any;
  category: any;
  subcategory: string;
  attrList: any = [];
  attrshow: boolean;
  formdata: any;
  attrData: any = {};
  response: any;
  // private catList$: Observable<any>;
  productid: void;
  formList: any = [];
  seq_no: any;
  sr_no: any;
  attrArray: any = [];
  obj: any = {};
  catList: any;
  productname: any;
  // masterDataForm: any;

  constructor(public globalService: GlobalServiceService, public dataService: DataServiceService, private ngxSmartService: NgxSmartModalService, private spinner: NgxSpinnerService) {
  }
  masterData: any = {};
  methodname: any;
  ngOnInit() {
    this.catList = this.dataService.getOnLoadServices('30');       //Categories List
    this.catSubcatList = this.dataService.getOnLoadServices('35'); // Sub categories List
  }


  // Get Attributes based on Category & subcategory 
  selectCat(data) {
    this.category = data;
  }
  selectSubcat(data) {
    this.subcategory = data;
    this.catAttrGet();
  }

  catAttrGet() {
    this.spinner.show();
    this.input_id = "32";
    this.attrshow = false;
    this.globalService.getDatawithQueryParams2(this.input_id, this.category, this.subcategory).subscribe((data) => {
      this.spinner.hide();
      this.attrList = data;
      if (data.length > 0) {
        this.attrshow = true;
      }
    },
      error => {
        this.ngxSmartService.getModal('errorModal').open();
        // console.log(error);
      });
  }


  //Attribute Values Submit- using (blur)
  myMethod(item, val) {
    this.attrArray.push({ "atrname": item.attribute, "atrvalue": val, "seq_no": item.seq_no });
    this.obj = this.attrArray;

  }


  // Form Submit 

  onSubmit(form: NgForm) {
    // this.productData(form);
    this.atributesData(form);
  }


  // products Post 

  productData(form) {
    this.spinner.show();
    this.formdata = { "flag": 1, "data": [this.masterData] };
    this.methodname = "product_master_data/"
    this.globalService.postData(this.formdata, this.methodname).subscribe((data) => {
      this.spinner.hide();
      console.log(data);
      if (data['status'] == "success") {
        // this.productid = data['user'].productid;
        // this.productname = data['user'].long_name

        this.atributesData(form);
      }
    },
      error => {
        this.ngxSmartService.getModal('errorModal').open();
        // console.log(error);
      });
  }



  // Attributes Post

  atributesData(form) {
    this.spinner.show();
    for (let i = 0; i < this.obj.length; i++) {
      this.obj[i].productid = this.masterData.productid;
      this.obj[i].product_name = this.masterData.long_name;
    }
    this.formdata = { "flag": 2, "data": this.obj };

    this.methodname = "product_master_data/"
    this.globalService.postData(this.formdata, this.methodname).subscribe((data) => {
      this.spinner.hide();
      console.log(data);
      if (data == "Success") {
        $("#successModal").modal('show');
        form.reset();
      }

    },
      error => {
        this.ngxSmartService.getModal('errorModal').open();
        // console.log(error);
      });

  }

}
