import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { DataServiceService } from "../../data-service.service";
import { GlobalServiceService } from "../../global-service.service";
import { NgxSmartModalService } from 'ngx-smart-modal';
import { NgxSpinnerService } from 'ngx-spinner';
declare var $: any;

@Component({
  selector: 'app-register-profile-info',
  templateUrl: './register-profile-info.component.html',
  styleUrls: ['./register-profile-info.component.scss']
})
export class RegisterProfileInfoComponent implements OnInit {
  profileData: any;
  activatedDataModel: any = {};
  categoryList: any;
  comapnyCodesList: any;
  message: any;
  loginUserData: any;

  constructor(private dataService: DataServiceService, private globalService: GlobalServiceService, private route: Router, private ngxSmartService: NgxSmartModalService, private spinner: NgxSpinnerService) { }

  ngOnInit() {
    this.loginUserData = JSON.parse(localStorage.getItem('loginUserData'));
    if(this.loginUserData===null){
      this.route.navigateByUrl('home');
    }
    this.profileData = JSON.parse(localStorage.getItem('userProfile'));
    console.log(this.profileData);

    this.comapnyCodesList = this.dataService.getOnLoadServices(3.1);
    this.categoryList = this.dataService.getOnLoadServices(50);
  }

  regProfileSave(status) {
    this.spinner.show();
    this.activatedDataModel.status = status;
    this.activatedDataModel.passing_param = 2;
    this.activatedDataModel.user_type = this.profileData.user_type;
    this.activatedDataModel.user_id = this.profileData.user_id;
    var methodName = 'api/registration/';
    this.globalService.postData(this.activatedDataModel, methodName).subscribe((data) => {
      this.spinner.hide();
      if (data.Status == 'Update sucessfully') {
        this.message = data.Status;
        //alert(data.Status);
        $('#editApproveModal').modal('show');
        //this.route.navigateByUrl('/users');
      } else {
        alert(data.Status);
      }
    },
      error => {
        this.ngxSmartService.getModal('errorModal').open();
        // console.log(error);
      });
  }

  gotoUsers() {
    this.route.navigateByUrl('/users');
  }


}
