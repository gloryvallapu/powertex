import { Component, OnInit, Input } from '@angular/core';
import { Router } from '@angular/router';
import { GlobalServiceService } from '../../global-service.service';
import { DataServiceService } from "../../data-service.service";
import { NgxSmartModalService } from 'ngx-smart-modal';
import { NgxSpinnerService } from 'ngx-spinner';
declare var $: any;

@Component({
  selector: 'app-register-profile',
  templateUrl: './register-profile.component.html',
  styleUrls: ['./register-profile.component.scss']
})
export class RegisterProfileComponent implements OnInit {
  regList: any = [];
  p: any = 1;
  searchText: any;
  rejectData: any = {};
  rejData: any;
  @Input() underProcessId: number;
  passingParam: any;
  alert: boolean;
  wish_alert: any;
  icon: boolean;


  constructor(private route: Router, private profileService: GlobalServiceService,
    private dataService: DataServiceService, public ngxSmartService: NgxSmartModalService, private spinner: NgxSpinnerService) {
  }

  ngOnInit() {
    this.alert = false;
    if (this.underProcessId == 1) {
      this.passingParam = 'UP'
    } else {
      this.passingParam = 'P'
    }
    this.getRegisterdUsers();
  }

  getRegisterdUsers() {
    this.spinner.show();
    this.profileService.getDatawithQueryParams1(3.5, this.passingParam).subscribe((data) => {
      this.spinner.hide();
      if (data.length > 0)
        this.regList = data;
      else {
        //alert('No data');
        // this.wish_alert = "No data"
        // this.addwish();
      }
    },
      error => {
        this.ngxSmartService.getModal('errorModal').open();
        // console.log(error);
      });
  };

  reject(data) {
    this.rejData = data;
    $('#confirmModal').modal('show');
  };

  confirm() {
    this.spinner.show();
    $('#confirmModal').modal('hide');
    this.rejectData.status = 'R';
    this.rejectData.passing_param = 2;
    this.rejectData.user_type = this.rejData.user_type;
    this.rejectData.user_id = this.rejData.user_id;
    var methodName = 'api/registration/';
    this.profileService.postData(this.rejectData, methodName).subscribe((data) => {
      this.spinner.hide();
      if (data.Status == 'Update sucessfully') {
        // alert(data.Status);
        this.wish_alert = data.Status;
        this.addwish();
        this.icon = true;
        this.getRegisterdUsers();
      } else {
        //alert('Error');
        this.wish_alert = "Error"
        this.addwish();
        this.icon = true;
      }
    },
      error => {
        this.ngxSmartService.getModal('errorModal').open();
        // console.log(error);
      });
  }

  addwish() {
    this.alert = true;
    setInterval(() => {
      this.alert = false;
    }, 5000);
  }

  viewMore(data) {
    this.ngxSmartService.resetModalData('viewmoreModal');
    this.ngxSmartService.setModalData(data, 'viewmoreModal');
    this.ngxSmartService.getModal('viewmoreModal').open();
  };

  approve(data) {
    this.dataService.saveData(data);
    localStorage.setItem('userProfile', JSON.stringify(data));
    this.route.navigate(['/register-profile-info']);
  };

}
