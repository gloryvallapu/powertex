import { Component, OnInit, Input } from '@angular/core';
import { GlobalServiceService } from "../../global-service.service";
import { Router } from "@angular/router";
import { NgxSmartModalService } from 'ngx-smart-modal';
import { NgxSpinnerService } from 'ngx-spinner';

@Component({
  selector: 'app-reject-users',
  templateUrl: './reject-users.component.html',
  styleUrls: ['./reject-users.component.scss']
})
export class RejectUsersComponent implements OnInit {
  internalRejectUsersList: any = [];
  p: any = 1;
  @Input() id: any;
  input_id: number;
  searchText: any;

  constructor(private globalServicce: GlobalServiceService, private route: Router, public ngxSmartService: NgxSmartModalService, private spinner: NgxSpinnerService) { }

  ngOnInit() {
    if (this.id == 1) {
      this.input_id = 3.5;
    } else {
      this.input_id = 3.6;
    }
    // console.log(id);
    this.getRejectUsers();
  };

  getRejectUsers() {
    this.spinner.show();
    this.globalServicce.getDatawithQueryParams1(this.input_id, 'R').subscribe((data) => {
      this.spinner.hide();
      if (data.length > 0) {
        this.internalRejectUsersList = data;
      } else {
        this.internalRejectUsersList = [];
      }
    },
      error => {
        this.ngxSmartService.getModal('errorModal').open();
        // console.log(error);
      });
  };


}
