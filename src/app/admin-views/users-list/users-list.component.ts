import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-users-list',
  templateUrl: './users-list.component.html',
  styleUrls: ['./users-list.component.scss']
})
export class UsersListComponent implements OnInit {
  openTab: any = 'Pending';
  loginUserData: any;

  constructor( private router:Router) { }

  ngOnInit() {
    this.loginUserData = JSON.parse(localStorage.getItem('loginUserData'));
    if(this.loginUserData===null){
      this.router.navigateByUrl('home');
    }
  };

  activate(tab) {
    this.openTab = tab;
  }

}
