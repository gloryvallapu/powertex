import { Component, OnInit } from '@angular/core';
import { Http } from '@angular/http';
import { GlobalServiceService } from 'src/app/global-service.service';
import { NgxSmartModalService } from 'ngx-smart-modal';

@Component({
  selector: 'app-users-reviews',
  templateUrl: './users-reviews.component.html',
  styleUrls: ['./users-reviews.component.scss']
})
export class UsersReviewsComponent implements OnInit {
  loginUserData: any;
  token: any;
  All_Rev:boolean=true;
  Approve_Rev:boolean=false;
  Rejected_Rev:boolean=false;
  reviews: any;

  constructor(private http:Http, private service:GlobalServiceService,private ngxSmartService: NgxSmartModalService) { }
  ngOnInit() {
    this.token = localStorage.getItem('token');
    this.loginUserData = JSON.parse(localStorage.getItem('loginUserData'));
    console.log(this.loginUserData);
    this.get_A_rev();
  }
  
  get_A_rev() {
    let param1="";
    let permission=0;
    return this.service.getDatawithQueryParams2('8.0',param1,permission ).subscribe((resp: Response) => {
    console.log(resp);

    this.reviews = resp; console.log("rev", this.reviews);
  },
    error => {
      this.ngxSmartService.getModal('errorModal').open();
      // console.log(error);
    });
    
  }
  A_Rev(){
    console.log("--");
  }
  Apr_Rev(){
    console.log("--");
  }

  Rej_Rev(){
    console.log("--");
  }
}
