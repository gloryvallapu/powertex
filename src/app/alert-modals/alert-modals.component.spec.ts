import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AlertModalsComponent } from './alert-modals.component';

describe('AlertModalsComponent', () => {
  let component: AlertModalsComponent;
  let fixture: ComponentFixture<AlertModalsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AlertModalsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AlertModalsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
