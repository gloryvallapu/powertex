import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { RouterModule, Routes } from "@angular/router";
import { NgxSmartModalModule, NgxSmartModalService } from "ngx-smart-modal";
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { HttpModule } from "@angular/http";
import { NgxPaginationModule } from 'ngx-pagination';
import { Ng2SearchPipeModule } from 'ng2-search-filter';
import { NgxSpinnerModule, NgxSpinnerService } from 'ngx-spinner';
import { TabsModule } from 'ngx-tabset';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { MaterialModule } from './material/material.module';
import 'zone.js/dist/zone.js';
import { GlobalServiceService } from "./global-service.service";
import { DataServiceService } from "./data-service.service";
import {NgbModule} from '@ng-bootstrap/ng-bootstrap';

// import {LocationStrategy, Location, PathLocationStrategy} from '@angular/common';

import { AppComponent } from './app.component';
import { HeaderComponent } from './header/header.component';
import { FooterComponent } from './footer/footer.component';
import { HomeComponent } from './home/home.component';
import { ProductPricingComponent } from './admin-views/product-pricing/product-pricing.component';
import { ProductMenuComponent } from './product-menu/product-menu.component';
import { LoginModalComponent } from './authentcation-views/login-modal/login-modal.component';
import { RegisterComponent } from './authentcation-views/register-step1/register.component';
import { SampleComponent } from './sample/sample.component';
import { RegisterProfileComponent } from './admin-views//register-profile/register-profile.component';
import { RegisterProfileInfoComponent } from './admin-views//register-profile-info/register-profile-info.component';
import { OtpModalComponent } from './authentcation-views/otp-modal/otp-modal.component';
import { AdminDashboardComponent } from './admin-views/admin-dashboard/admin-dashboard.component';
import { MastersCompanyComponent } from './admin-views/masters-company/masters-company.component';
import { MasterDataUploadComponent } from './admin-views/master-data-upload/master-data-upload.component';
import { ProductExcelUploadComponent } from './admin-views/product-excel-upload/product-excel-upload.component';
import { ViewProductsDataComponent } from './view-products-data/view-products-data.component';
import { InternalUsersComponent } from './admin-views/internal-users/internal-users.component';
import { InternalUsersListComponent } from './admin-views/internal-users-list/internal-users-list.component';
import { InternalUserEditComponent } from './admin-views/internal-user-edit/internal-user-edit.component';
import { UniquePipe } from './unique.pipe';
import { SubbComponent } from './subb/subb.component';
import { CategoryListComponent } from './category-list/category-list.component';
import { Category } from './category-list/category';
import { ProductDetailComponent } from './product-detail/product-detail.component';
import { ActiveUsersComponent } from './admin-views/active-users/active-users.component';
import { DealerOrdersComponent } from './dealer/dealer-orders/dealer-orders.component';
import { DealerHeaderComponent } from './dealer/dealer-header/dealer-header.component';
import { ViewCartComponent } from './view-cart/view-cart.component';
import { DealerDashboardComponent } from './dealer/dealer-dashboard/dealer-dashboard.component';
import { SearchPipe } from './search.pipe';
import { PackingListComponent } from './wh-manager/packing-list/packing-list.component';
import { UsersListComponent } from './admin-views/users-list/users-list.component';
import { RejectUsersComponent } from './admin-views/reject-users/reject-users.component';
import { MakePackingListComponent } from './wh-manager/make-packing-list/make-packing-list.component';
import { AlertModalsComponent } from './alert-modals/alert-modals.component';
import { ViewMoreComponent } from './admin-views/view-more/view-more.component';
import { PayuComponent } from './payu/payu.component';
import { GoodsReceiptNoteComponent } from './dealer/goods-receipt-note/goods-receipt-note.component';
import { CheckoutPageComponent } from './checkout-page/checkout-page.component';
import { GetProformaListComponent } from './accountant/get-proforma-list/get-proforma-list.component';
import { MakeInvoiceComponent } from './accountant/make-invoice/make-invoice.component';
import { StepperComponent } from './stepper/stepper.component';
import { GoodsReceiptDetailComponent } from './dealer/goods-receipt-detail/goods-receipt-detail.component';
import { Ng5SliderModule } from 'ng5-slider';
import { PosUsersComponent } from './pos/pos-users/pos-users.component';
import { PosNewUserComponent } from './pos/pos-new-user/pos-new-user.component';
import { GoodsReceiptNotePrintComponent } from './dealer/goods-receipt-note-print/goods-receipt-note-print.component';
import { PurchaseOrderPrintComponent } from './wh-manager/purchase-order-print/purchase-order-print.component';
import { UserProfileComponent } from './user-profile/user-profile.component';
import { GetChallanListComponent } from './wh-manager/get-challan-list/get-challan-list.component';
import { MakeChallanComponent } from './wh-manager/make-challan/make-challan.component';
import { AboutUsComponent } from './about-us/about-us.component';
import { CarrersComponent } from './carrers/carrers.component';
import { ContactUsComponent } from './contact-us/contact-us.component';
import { SampleHomeComponent } from './sample-home/sample-home.component';
import { AccountantDashboardComponent } from './accountant/accountant-dashboard/accountant-dashboard.component';
import { WhDashboardComponent } from './wh-manager/wh-dashboard/wh-dashboard.component';
import { MinDirective } from './directives/min.directive';
import { MaxDirective } from './directives/max.directive';
import { Broadcaster } from './app.broadcaster';
import { ComponentCommunicationService } from './component-communication.service';
import { StoreLocationComponent } from './store-location/store-location.component';
import { CatgMenuComponent } from './catg-menu/catg-menu.component';
import { DashboardComponent } from './dashboard/dashboard.component';
import { ProdCatgegoryComponent } from './prod-catgegory/prod-catgegory.component';
import { ProductMenu2Component } from './product-menu2/product-menu2.component';
import { SuccessComponent } from './success/success.component';
import { SearchListComponent } from './search-list/search-list.component';
import { DealerCategoryComponent } from './dealer/dealer-category/dealer-category.component';
import { WishListComponent } from './wish-list/wish-list.component';
import { MyOrdersComponent } from './my-orders/my-orders.component';
import { LoginComponent } from './login/login.component';
import { BreadcrumbComponent } from './breadcrumb/breadcrumb.component';
import { ErrorModalComponent } from './authentcation-views/error-modal/error-modal.component';
import { DealerStatusComponent } from './dealer/dealer-status/dealer-status.component';
import { WhStatusComponent } from './wh-manager/wh-status/wh-status.component';
import { ImportPoComponent } from './product-manager/import-po/import-po.component';
import { ImportGrnComponent } from './product-manager/import-grn/import-grn.component';
import { PmStatusComponent } from './product-manager/pm-status/pm-status.component';
import { ImportInvoiceComponent } from './product-manager/import-invoice/import-invoice.component';
import { ImportMakeInvoiceComponent } from './product-manager/import-make-invoice/import-make-invoice.component';
import { FeedbackModalComponent } from './feedback-modal/feedback-modal.component';
import { RatingComponent } from './rating/rating.component';
import { CategoryAllComponent } from './category-all/category-all.component';
import { GenerateImportGrnComponent } from './product-manager/generate-import-grn/generate-import-grn.component';
import { UsersReviewsComponent } from './admin-views/users-reviews/users-reviews.component';
import { InvoicePrintComponent } from './accountant/invoice-print/invoice-print.component';
import { AccStatusComponent } from './accountant/acc-status/acc-status.component';
import { WhStockComponent } from './wh-manager/wh-stock/wh-stock.component';
import { WhReportsComponent } from './wh-manager/wh-reports/wh-reports.component';
import { ImportInvoicePrintComponent } from './product-manager/import-invoice-print/import-invoice-print.component';
import { PackingListPrintComponent } from './wh-manager/packing-list-print/packing-list-print.component';
import { ChallanPrintComponent } from './wh-manager/challan-print/challan-print.component';




const approot: Routes = [
  { path: "home", component: HomeComponent, data: { breadcrumb: 'home' } },
  { path: "wishlist", component: WishListComponent, data: { breadcrumb: 'wishlist' } },
  { path: "register-profile", component: RegisterProfileComponent, data: { breadcrumb: 'profile' } },
  { path: "admin-dashboard", component: AdminDashboardComponent, data: { breadcrumb: 'admin-dashboard' } },
  { path: "dealer-dashboard", component: DealerDashboardComponent },
  { path: "register-profile-info", component: RegisterProfileInfoComponent, data: { breadcrumb: 'profile' } },
  { path: "masters-company", component: MastersCompanyComponent, data: { breadcrumb: 'profile' } },
  { path: "master-data-upload", component: MasterDataUploadComponent },
  { path: "header", component: HeaderComponent },
  { path: "footer", component: FooterComponent },
  { path: "product-pricing", component: ProductPricingComponent },
  { path: "registration", component: RegisterComponent },
  { path: "registration/:id", component: RegisterComponent },
  { path: "product", component: ProductMenuComponent },
  { path: "sample", component: SampleComponent },
  { path: "internal-users", component: InternalUsersComponent },
  { path: "internal-user-edit", component: InternalUserEditComponent },
  { path: "sub", component: SubbComponent },
  { path: "category/:b/:c/:d/:e", component: CategoryListComponent },
  { path: "category/:search", component: CategoryListComponent },
  { path: "search/:search", component: SearchListComponent },
  { path: "product-detail/:category/:sub_category/:model", component: ProductDetailComponent },
  { path: "active-users", component: ActiveUsersComponent },
  { path: "dealer-order", component: DealerOrdersComponent },
  { path: "viewcart", component: ViewCartComponent },
  { path: "purchase-orders-list", component: PackingListComponent },
  { path: "users", component: UsersListComponent },
  { path: "packing-List", component: MakePackingListComponent },
  { path: "payu", component: PayuComponent },
  { path: "grn", component: GoodsReceiptNoteComponent },
  { path: "grn-print", component: GoodsReceiptNotePrintComponent },
  { path: "checkout-page", component: CheckoutPageComponent },
  { path: "profomaList", component: GetProformaListComponent },
  { path: "invoice", component: MakeInvoiceComponent },
  { path: "invoice-Print", component: InvoicePrintComponent },
  { path: "about-us", component: AboutUsComponent },
  { path: "carrers", component: CarrersComponent },
  { path: "contact-us", component: ContactUsComponent },
  { path: "dashboard", component: DashboardComponent },
  { path: "pos", component: PosUsersComponent },
  { path: 'grn_detail/:invoice_no/:invoice_date/:company_code/:packing_date', component: GoodsReceiptDetailComponent },
  { path: 'po-print', component: PurchaseOrderPrintComponent },
  { path: "store-location", component: StoreLocationComponent },
  { path: "checkout/:id", component: StepperComponent },
  { path: "profile", component: UserProfileComponent },
  { path: "challan-list", component: GetChallanListComponent },
  { path: "challan-generation", component: MakeChallanComponent },
  { path: "accountant-dashboard", component: AccountantDashboardComponent },
  { path: "warehouse-dashboard", component: WhDashboardComponent },
  { path: "prod-category/:category", component: ProdCatgegoryComponent },
  { path: "nmenu", component: ProductMenu2Component },
  { path: "dealer-catg", component: DealerCategoryComponent },
  { path: "myOrders", component: MyOrdersComponent },
  { path: "dealer-status", component: DealerStatusComponent },
  { path: "wh-status", component: WhStatusComponent },
  { path: "product-manager-PO", component: ImportPoComponent },
  { path: "product-manager-GRN", component: ImportGrnComponent },
  { path: "product-manager-Status", component: PmStatusComponent },
  { path: "product-manager-Invoice", component: ImportInvoiceComponent },
  { path: "product-Invoice", component: ImportMakeInvoiceComponent },
  { path: "product-GRN", component: GenerateImportGrnComponent },
  // { path: "rating",component:RatingComponent},
  { path:"all-Category",component:CategoryAllComponent},
  { path:"user-review",component:UsersReviewsComponent},
  { path:"packing_list_print",component:PackingListPrintComponent},
  { path:"accounts-status",component:AccStatusComponent},
  { path:"challan_print",component:ChallanPrintComponent},
  { path: "", redirectTo: "/home", pathMatch: "full", data: { breadcrumbs: 'home' } }
  
];

@NgModule({
  declarations: [
    AppComponent,
    HeaderComponent,
    FooterComponent,
    HomeComponent,
    ProductPricingComponent,
    LoginModalComponent,
    RegisterComponent,
    ProductMenuComponent,
    SampleComponent,
    RegisterProfileComponent,
    RegisterProfileInfoComponent,
    OtpModalComponent,
    AdminDashboardComponent,
    MastersCompanyComponent,
    MasterDataUploadComponent,
    ProductExcelUploadComponent,
    ViewProductsDataComponent,
    InternalUsersComponent,
    InternalUsersListComponent,
    InternalUserEditComponent,
    UniquePipe,
    SubbComponent,
    CategoryListComponent,
    ProductDetailComponent,
    ActiveUsersComponent,
    ProductDetailComponent,
    DealerOrdersComponent,
    DealerHeaderComponent,
    DealerDashboardComponent,
    ViewCartComponent,
    SearchPipe,
    PackingListComponent,
    UsersListComponent,
    RejectUsersComponent,
    AlertModalsComponent,
    ViewMoreComponent,
    MakePackingListComponent,
    AlertModalsComponent,
    PayuComponent,
    GoodsReceiptNoteComponent,
    CheckoutPageComponent,
    GetProformaListComponent,
    MakeInvoiceComponent,
    StepperComponent,
    GoodsReceiptDetailComponent,
    PosUsersComponent,
    PosNewUserComponent,
    GoodsReceiptNotePrintComponent,
    PurchaseOrderPrintComponent,
    UserProfileComponent,
    GetChallanListComponent,
    MakeChallanComponent,
    AboutUsComponent,
    CarrersComponent,
    ContactUsComponent,
    SampleHomeComponent,
    AccountantDashboardComponent,
    WhDashboardComponent,
    MinDirective,
    MaxDirective,
    StoreLocationComponent,
    CatgMenuComponent,
    DashboardComponent,
    ProdCatgegoryComponent,
    SuccessComponent,
    WishListComponent,
    ProductMenu2Component,
    SuccessComponent,
    SearchListComponent,
    DealerCategoryComponent,
    MyOrdersComponent,
    LoginComponent,
    BreadcrumbComponent,
    ErrorModalComponent,
    DealerStatusComponent,
    WhStatusComponent,
    ImportPoComponent,
    ImportGrnComponent,
    PmStatusComponent,
    ImportInvoiceComponent,
    ImportMakeInvoiceComponent,
    FeedbackModalComponent,
    RatingComponent,
    CategoryAllComponent,
    GenerateImportGrnComponent,
    UsersReviewsComponent,
    InvoicePrintComponent,
    AccStatusComponent,
    PackingListPrintComponent,
    ChallanPrintComponent,
    WhStockComponent,
    WhReportsComponent,
    ImportInvoicePrintComponent,
    PackingListPrintComponent
  ],

  imports: [
    BrowserModule,
    FormsModule,
    NgxSmartModalModule,
    HttpModule,
    NgxPaginationModule,
    Ng2SearchPipeModule,
    NgxSpinnerModule,
    Ng5SliderModule,
    // RouterModule.forRoot(approot),
    BrowserAnimationsModule,
    MaterialModule,
    ReactiveFormsModule,
    NgbModule,
    RouterModule.forRoot(approot, { useHash: true }),
    TabsModule.forRoot(),
   
  ],
  providers: [NgxSpinnerService, Category, NgxSmartModalService, GlobalServiceService, DataServiceService, Broadcaster, ComponentCommunicationService],
  bootstrap: [AppComponent]
})
export class AppModule {


}

