import { Component, OnInit } from '@angular/core';
import { NgxSmartModalService } from "ngx-smart-modal";
import { NgxSpinnerService } from 'ngx-spinner';
// import { Observable } from 'rxjs';
// declare var $: any;

@Component({
  selector: 'app-error-modal',
  templateUrl: './error-modal.component.html',
  styleUrls: ['./error-modal.component.scss']
})
export class ErrorModalComponent implements OnInit {

  constructor(public ngxSmartModalService: NgxSmartModalService, public spinner: NgxSpinnerService) {
    this.spinner.hide();
  }

  ngOnInit() {

  }


}
