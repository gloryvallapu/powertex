import { Component, OnInit } from '@angular/core';
import { NgxSmartModalService } from "ngx-smart-modal";

@Component({
  selector: 'app-otp-modal',
  templateUrl: './otp-modal.component.html',
  styleUrls: ['./otp-modal.component.scss']
})
export class OtpModalComponent implements OnInit {
  otpModal: any = {};
  constructor(private modalService: NgxSmartModalService) { }

  ngOnInit() {
  };

  submitOTP(otp) {
    console.log(otp);
    this.modalService.close('otpModal');
  }

}
