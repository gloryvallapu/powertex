import { Component, OnInit, Input } from '@angular/core';
import { Router } from "@angular/router";
//import 'bootstrap';
import { ActivatedRoute } from '@angular/router';
import { GlobalServiceService } from "../../global-service.service";
import { DataServiceService } from "../../data-service.service";
import { NgxSmartModalService } from "ngx-smart-modal";
import { NgxSpinnerService } from 'ngx-spinner';
import { ComponentCommunicationService } from '../.././component-communication.service';

declare var $: any;

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.scss']
})
export class RegisterComponent implements OnInit {
  association: any;
  userDataArray: any = [];
  regCommonData: any;
  userTypeId: any;
  response: any;
  response1: any;
  response2: any;
  state: any;
  response3: any;
  countriesList: any;
  country: any;
  routeParams: any;
  designationsList: any;
  comapnyCodesList: any;
  registerModel: any = {};
  reg1: any = true;
  dealerReg: any = false;
  regData: any = [];
  userRegModel: any = {};
  otpModal: any = {};
  obj: any = {};
  @Input() styleId: number;
  mailValidation: boolean = false;
  mobileValidation: boolean = false;
  message: any;
  alert: boolean;
  wish_alert: any;
  icon: boolean;

  constructor(private route: Router, private regService: GlobalServiceService,
    private activeRoute: ActivatedRoute, private dataService: DataServiceService,
    private ngxSmartService: NgxSmartModalService, private eventEmit: ComponentCommunicationService, private spinner: NgxSpinnerService) {
    this.activeRoute.params.subscribe(params => {
      this.routeParams = params.id;
    },
      error => {
        this.ngxSmartService.getModal('errorModal').open();
        // console.log(error);
      });
  }

  ngOnInit() {
    this.alert = false;
    window.scrollTo(0, 0);
    this.country = 'INDIA';
    this.obj.id = 3;
    if (this.routeParams == 1) { // 1 for InternalUsers(Employee reg)
      this.userTypeId = 37;
    } else {
      this.userTypeId = 38;
      this.designationsList = this.dataService.getOnLoadServices(36);
      this.comapnyCodesList = this.dataService.getOnLoadServices(3.1);
    }
    this.getOnLoadServices(this.userTypeId, '19');
    this.getDatawith1Param('15', this.country);
  };

  getOnLoadServices(param1, param2) {
    this.spinner.show();
    this.regService.forkJoinMethodForInputID1(param1, param2).subscribe((data) => {
      this.spinner.hide();
      this.response = data[0];
      this.countriesList = data[1];
      this.userRegModel.country = data[1][0].country;
    },
      error => {
        this.ngxSmartService.getModal('errorModal').open();
        // console.log(error);
      });
  };


  onSelectCountry(country) {
    this.country = country;
    this.getDatawith1Param('15', country);
  }


  getDatawith1Param(input_id, param) {
    this.spinner.show();
    this.regService.getDatawithQueryParams1(input_id, param).subscribe((data) => {
      this.spinner.hide();
      this.response1 = data;
    },
      error => {
        this.ngxSmartService.getModal('errorModal').open();
        // console.log(error);
      });
  };

  onSelectState(state) {
    this.state = state;
    this.getDatawith2Param('16', this.country, state);
  }
  getDatawith2Param(input_id, param1, param2) {
    this.spinner.show();
    this.regService.getDatawithQueryParams2(input_id, param1, param2).subscribe((data) => {
      this.spinner.hide();
      this.response2 = data;
    },
      error => {
        this.ngxSmartService.getModal('errorModal').open();
        // console.log(error);
      });
  };

  onSelectDistrict(district) {
    this.getDatawith3Param('17', this.country, this.state, district);
  };

  getDatawith3Param(input_id, param1, param2, param3) {
    this.spinner.show();
    this.regService.getDatawithQueryParams3(input_id, param1, param2, param3).subscribe((data) => {
      this.spinner.hide();
      this.response3 = data;
    },
      error => {
        this.ngxSmartService.getModal('errorModal').open();
        // console.log(error);
      });
  };



  resetForm(form) {
    form.reset();
  };



  gotoNext() {
    if (this.registerModel.cnfPassword != this.registerModel.password) {
      //alert('conform password not matched');
      this.wish_alert = "Password and Confirm Password should match"
      this.addwish();
      // this.icon = true;

    } else if (this.registerModel.cnfPassword == this.registerModel.password) {
      this.association = this.registerModel.user_type;
      this.reg1 = false;
      this.dealerReg = true;
    }

  };

  gotoPrevious() {
    this.reg1 = true;
    this.dealerReg = false;
  };

  regSubmit(form) {
    // if (this.userRegModel.cnfPassword != this.userRegModel.password) {
      this.spinner.show();
    this.otpModal = {};
    this.registerModel.passing_param = 1;
    if (this.registerModel.user_type == 'Customer') {
      var regMoethod = 'sendotp/';
      this.regService.getDatawithQuery(regMoethod, this.registerModel.mobile).subscribe((data) => {
        this.spinner.hide();
        if (data.status == 'otp sended !!!') {
          $("#otpModal").modal('show');
        } else {
          alert(data.status);
        }
      },
        error => {
          this.ngxSmartService.getModal('errorModal').open();
          // console.log(error);
        });
    } else {

      this.submitReg(form);
    }

  };

  addwish() {
    this.alert = true;
    setInterval(() => {
      this.alert = false;
    }, 5000);
  }

  submitReg(form) {
    this.spinner.show();
    if (this.registerModel.user_type == 'Customer' || this.registerModel.user_type == 'Employee') {
      this.registerModel.status = 'A';
      if (this.registerModel.user_type == 'Customer') {
        this.registerModel.category_profile = 'EU3';
      }
    } else {
      this.registerModel.status = 'P';
    }
    console.log(this.registerModel);

    var body = Object.assign({}, this.registerModel, this.userRegModel);
    var regMoethod = 'api/registration/';
    this.regService.postData(body, regMoethod).subscribe((data) => {
      this.spinner.hide();
      if (data.Status == 'Success .. ') {
        //alert('You have Successfully Registered In');
        $("#otpStatusokModal").modal('show');
        // if (this.registerModel.user_type != 'Employee') {
        //   this.ngxSmartService.getModal('loginModal').open();
        // }
      } else {
        alert(data.Status);
      }
    }
      ,
      error => {
        this.ngxSmartService.getModal('errorModal').open();
        // console.log(error);
      }
    );
  };

  gotoPreviousreg() {
    // this.route.navigateByUrl("home");
    if (this.registerModel.user_type != 'Employee' && this.registerModel.user_type != 'Vendor') {
      this.ngxSmartService.getModal('loginModal').open();
    }
  }


  submitOTP(otp) {
    this.spinner.show();
    var regMoethod = 'sendotp/';
    var otpValidBody = { "otp": otp, "phone": this.registerModel.mobile };
    this.regService.postData(otpValidBody, regMoethod).subscribe((data) => {
      this.spinner.hide();
      if (data.status == 'otp verify !!!') {
        $("#otpModal").modal('hide');
        this.submitReg('form');
      } else {
        alert(data.status);
        // this.message = data.status;
       // $("#otpStatusModal").modal('show');
      }
    },
      error => {
        this.ngxSmartService.getModal('errorModal').open();
        // console.log(error);
      });
  };


  verifyUser(data, field) {
    this.spinner.show();
    this.regService.getDatawithQueryParams1(4.9, data).subscribe((data) => {
      this.spinner.hide();
      if (data.status == "1") {
        if (field == 'email') {
          this.mailValidation = true;
        } else {
          this.mobileValidation = true;
        }
      } else {
        if (field == 'email') {
          this.mailValidation = false;
        } else {
          this.mobileValidation = false;
        }
      }
    },
      error => {
        this.ngxSmartService.getModal('errorModal').open();
        // console.log(error);
      });
  }

}
