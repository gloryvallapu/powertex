import { Component, OnInit } from '@angular/core';
import { Location } from '@angular/common';
import { Router } from '@angular/router';
import { NgxSmartModalService } from 'ngx-smart-modal';

@Component({
  selector: 'app-breadcrumb',
  templateUrl: './breadcrumb.component.html',
  styleUrls: ['./breadcrumb.component.scss']
})
export class BreadcrumbComponent implements OnInit {

  route: string;
  breadcrumbList: Array<any> = [];
  routeLinks: number;
  count: number;

  constructor(location: Location, router: Router, private ngxSmartService: NgxSmartModalService) {
    router.events.subscribe((val) => {
      if (location.path() !== '') {
        this.route = location.path();
        this.breadcrumbList = this.route.split('/');
        //   this.breadcrumbList.push(this.route.split('/'));
        this.count = this.breadcrumbList.length;
      } else {
        this.route = '/home';
      }
    },
    error => {
      this.ngxSmartService.getModal('errorModal').open();
      // console.log(error);
    });
  }

  ngOnInit() { }

}
