import { Component, OnInit } from '@angular/core';
import { GlobalServiceService } from '../global-service.service';
import { Router } from '@angular/router';
import { NgxSmartModalService } from 'ngx-smart-modal';
import { NgxSpinnerService } from 'ngx-spinner';

@Component({
  selector: 'app-category-all',
  templateUrl: './category-all.component.html',
  styleUrls: ['./category-all.component.scss']
})
export class CategoryAllComponent implements OnInit {
  resources: any;

  constructor(private service:GlobalServiceService,private router:Router,private ngxSmartService:NgxSmartModalService, private spinner: NgxSpinnerService) { }

  ngOnInit() {
    this.getCatg();
  }
  getCatg(){
    this.spinner.show();
    return this.service.getDatawithInput_id('14').subscribe((resp: Response) => {  
      this.spinner.hide();
      this.resources = resp;  
      console.log(this.resources);
      
    },
    error => {
      this.ngxSmartService.getModal('errorModal').open();
      // console.log(error);
    });
  }
  selected_Cat(Category){
    this.router.navigate(['/prod-category',Category])
  }

}
