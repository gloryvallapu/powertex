import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { GlobalServiceService } from '../global-service.service';
// import { Category } from "../category-list/category";
import { DataServiceService } from '../data-service.service';
import { Options } from 'ng5-slider';
import { ComponentCommunicationService } from '../component-communication.service';
import { NgxSmartModalService } from 'ngx-smart-modal';
import { NgxSpinnerService } from 'ngx-spinner';
declare var $: any;

@Component({
  selector: 'app-category-list',
  templateUrl: './category-list.component.html',
  styleUrls: ['./category-list.component.scss']
})
export class CategoryListComponent implements OnInit {

  // public firstname: any;
  //   public lastname: any;

  //   public constructor(private route: ActivatedRoute) {
  //       this.route.queryParams.subscribe(params => {
  //         console.log(params);


  //           this.firstname = params["firstname"];
  //           this.lastname = params["lastname"];
  //           console.log(this.firstname);
  //       });
  //       console.log(this.firstname,this.lastname);

  //   }

  // ngOnInit() {
  // }

  Page: any = 1;
  category: any;
  sub_c: any;
  sub: any;
  d: any;
  e: any;
  f: any;
  resources2: any;
  option: any;
  modal: any = [];
  range: any = [];
  select: any;
  order: string;
  catgHidden: boolean = false;
  discount: number = 1;
  selected_disc: number = 2;
  search_val: any;
  wish_color = "#a09898";
  loginUserData: any;
  methodname: string;
  token: any;
  wish_alert: any;
  alert: boolean;
  obj: any = {};
  user_id: any;
  icon: boolean;
  constructor(private router: Router, private route: ActivatedRoute, private service: GlobalServiceService, public dataService: DataServiceService,
    private eventemit: ComponentCommunicationService, private ngxSmartService: NgxSmartModalService, private spinner: NgxSpinnerService) {
    this.obj.id = 4;
  }

  ngOnInit() {
    // $("#success-alert").hide();
    this.alert = false;
    this.token = localStorage.getItem('token');
    this.loginUserData = JSON.parse(localStorage.getItem('loginUserData'));
    this.sub = this.route.params.subscribe(params => {
      this.search_val = params['search'];
      if (this.token == null) {
        this.user_id = '';
      } else {
        this.user_id = this.loginUserData.user_id;
      }
      console.log("search", this.search_val);
      if (this.search_val === undefined) {
        this.d = params['b'];
        this.sub_c = params['c'];
        this.modal = atob(params['d']);
        this.select = atob(params['e']);
        // this.d=this.category;
        this.e = atob(this.sub_c);
        console.log(this.d, "", this.e, "", this.select, this.modal, this.user_id);
        this.getdata1();
      }
      else {
        this.getsearch();
      }

    },
      error => {
        this.ngxSmartService.getModal('errorModal').open();
        // console.log(error);
      });

  }
  getdata1() {
    this.spinner.show();
    return this.service.getDatawithQueryParams4User_id('10', this.d, this.e, this.select, this.modal, this.user_id).subscribe((resp: Response) => {
      this.spinner.hide();
      console.log(resp);

      this.resources2 = resp;
    },
      error => {
        this.ngxSmartService.getModal('errorModal').open();
        // console.log(error);
      });

  }
  getsearch() {
    this.spinner.show();
    return this.service.getDatawithQueryParams1('7.3', this.search_val).subscribe((resp: Response) => {
      this.spinner.hide();
      console.log(resp);


      this.resources2 = resp;
    },
      error => {
        this.ngxSmartService.getModal('errorModal').open();
        // console.log(error);
      });
  }

  //price Range
  minValue: number = 500;
  maxValue: number = 10000;
  options: Options = {
    floor: 500,
    ceil: 10000,
    step: 500
  };

  sub_cat(p) {
    console.log(p);
    // let category=btoa(p.category);
    // let sub_category=btoa(p.subcategory);
    // let model=btoa(p.modelno);
    let category = p.category;
    let sub_category = p.subcategory;
    let model = p.modelno;
    console.log("your Category", category, "hhh", sub_category, model);

    let obj = p;
    localStorage.setItem('key', JSON.stringify(obj));
    // console.log(p);
    // this.obj.setCategory(p);

    //  this.router.navigateByUrl('/product-detail');
    this.router.navigate(['/product-detail', category, sub_category, model]);
  }


  //price order-filter
  Ascend() {
    this.spinner.show();
    this.order = "";
    this.order = "acc";
    console.log(this.order);

    return this.service.getDatawithQueryParams7User_id('10', this.d, this.e, this.select, this.modal, this.order, this.range, this.discount, this.user_id).subscribe((resp: Response) => {
      this.spinner.hide();
      console.log(resp);

      this.resources2 = resp; console.log(this.resources2);
    },
      error => {
        this.ngxSmartService.getModal('errorModal').open();
        // console.log(error);
      });

  }

  Descend() {
    this.spinner.show();
    this.order = "";
    this.order = "dec";
    return this.service.getDatawithQueryParams7User_id('10', this.d, this.e, this.select, this.modal, this.order, this.range, this.discount, this.user_id).subscribe((resp: Response) => {
      this.spinner.hide();
      console.log(resp);

      this.resources2 = resp; console.log(this.resources2);
    },
      error => {
        this.ngxSmartService.getModal('errorModal').open();
        // console.log(error);
      });
  }


  //price-filter
  price_min(min, m) {
    this.spinner.show();
    this.range = [];
    // console.log(min);
    // console.log(m);
    let mini: number = +min;
    let maxi: number = +m
    this.range.push(mini);
    this.range.push(maxi);
    console.log("range-", this.range);
    let order = "";
    return this.service.getDatawithQueryParams7User_id('10', this.d, this.e, this.select, this.modal, this.order, this.range, this.discount, this.user_id).subscribe((resp: Response) => {
      this.spinner.hide();
      console.log(resp);

      this.resources2 = resp; console.log(this.resources2);
    },
      error => {
        this.ngxSmartService.getModal('errorModal').open();
        // console.log(error);
      });
  }

  price_max(min, m) {
    console.log(min);
    console.log(m);
  }

  price(min, max) {
    console.log(min, "--", max);

  }
  disp_catg() {
    this.catgHidden = !this.catgHidden;
  }

  disc(p) {
    this.spinner.show();
    console.log(p);
    this.selected_disc = p;
    this.discount = p;
    return this.service.getDatawithQueryParams7User_id('10', this.d, this.e, this.select, this.modal, this.order, this.range, this.discount, this.user_id).subscribe((resp: Response) => {
      this.spinner.hide();
      console.log(resp);

      this.resources2 = resp; console.log(this.resources2);
    },
      error => {
        this.ngxSmartService.getModal('errorModal').open();
        // console.log(error);
      });

  }

  //wishlist-code
  addwish_list(obj, checkval) {
    this.spinner.show();
    let body = { "s.no": obj.seq_no, "productid": obj.productid, "user_id": this.loginUserData.user_id, "wishlist": checkval }
    console.log(body)
    this.methodname = "wishlist_insert/";
    this.service.postData(body, this.methodname).subscribe(data => {
      this.spinner.hide();
      console.log(data);
      if (data.Status == 1) {
        this.wish_alert = "Added to wishlist."
        this.icon = true;
        this.obj.wishList_count = data.count;
        this.eventemit.fire(this.obj);
        this.addwish();
        this.getdata1();
      } else if (data.Status == 0) {
        this.obj.wishList_count = data.count;
        this.eventemit.fire(this.obj);
        this.wish_alert = "Removed  from wishList"
        this.icon = false;
        this.addwish();
        //  alert('removed  from wishList');
        this.getdata1();
      }
    },
      error => {
        this.ngxSmartService.getModal('errorModal').open();
        // console.log(error);
      });
  }
  // addwish() {
  //   $("#success-alert").fadeTo(2000, 500).slideUp(500, function(){
  //  $("#success-alert").slideUp(500);
  //   });  
  // }
  addwish() {
    //   $("#success-alert").fadeTo(2000, 500).slideUp(500, function(){
    //  $("#success-alert").slideUp(500);
    //   });  
    this.alert = true;
    setInterval(() => {
      this.alert = false;
    }, 5000);
  }

  wish_list(obj, checkval) {
    if (this.token != '' && this.token != undefined) {
      this.addwish_list(obj, checkval)
    } else {
      this.ngxSmartService.getModal('loginModal').open();
    }

  }
  catg_crumb(d) {
    console.log(d);
    let category = d;
    this.router.navigate(['/prod-category', category])

  }

}
// setCategory(obj)