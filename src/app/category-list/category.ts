export interface Icategory { 
    productid ?: any,
    gst ?: any,
     mrp ?: any,
     name3 ?: any,
     modelno ?: any,
     category ?: any,
     discount ?: any,
     net_price ?: any,
     low_image_1 ?: any,
     subcategory ?: any,
     high_image_1 ?: any,
     high_image_2 ?: any,
     high_image_3 ?: any,
     high_image_4 ?: any,
     high_image_5 ?: any,
     high_image_6 ?: any
 } 
 

 export class Category  {
    category:Icategory={};
   
 
   setCategory(obj){
     this.category.productid=obj.productid;
     this.category.gst=obj.gst;
     this.category.mrp=obj.mrp;
     this.category.name3=obj.name3;
     this.category.modelno=obj.modelno;
     this.category.category=obj.category;
     this.category.discount=obj.discount;
     this.category.net_price=obj.net_price;
     this.category.low_image_1=obj.low_image_1;
     this.category.subcategory=obj.subcategory;
     this.category.high_image_1=obj.high_image_1;
     this.category.high_image_2=obj.high_image_2;
     this.category.high_image_3=obj.high_image_3;
     this.category.high_image_4=obj.high_image_4;
     this.category.high_image_5=obj.high_image_5;
     this.category.high_image_6=obj.high_image_6;
   }
 
   getCategory(){
     return this.category
   }
 }
 
 