import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-checkout-page',
  templateUrl: './checkout-page.component.html',
  styleUrls: ['./checkout-page.component.scss']
})
export class CheckoutPageComponent implements OnInit {
  newAddress: boolean = false;
  checkout: any = {};
  showLogin: boolean = true;
  showOrderSummary: boolean = false;
  showAddress: boolean = false;
  showPayments: boolean = false;
  loginUserData: any;
  usertype: string;
  guestAddress: boolean;


  constructor() {
   }

  ngOnInit() {
    this.loginUserData = JSON.parse(localStorage.getItem('loginUserData'));
    console.log(this.loginUserData);
    if (this.loginUserData == undefined) {
      this.showLogin = true;
    } else {
     
      if(this.loginUserData.user_type =="Customer"){
        this.showAddress = true;
        this.showLogin = false;
        console.log(this.showAddress)
      }else if(this.loginUserData.user_type =="Guest"){
        // this.showAddress = true;
        this.guestAddress = true;
        this.showLogin = false;
        console.log(this.guestAddress);

      }
      
    }
  }

  addNewAdd() {
    this.newAddress = true;
  };

  cancelNewAdd() {
    this.newAddress = false;
  };

  gotoOrderSummary() {
    this.showOrderSummary = true;
    this.showAddress = false;
  };
  gotoPayment() {
    this.showOrderSummary = false;
    this.showPayments = true;
  }

}
