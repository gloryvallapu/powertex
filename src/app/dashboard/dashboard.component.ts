import { Component, OnInit } from '@angular/core';
import { Router } from "@angular/router";

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.scss']
})
export class DashboardComponent implements OnInit {
  token: string;
  loginUserData: any;

  constructor(private route: Router) { }

  ngOnInit() {
    // this.token = localStorage.getItem('token');
    this.loginUserData = JSON.parse(localStorage.getItem('loginUserData'));
    console.log("status111",this.loginUserData);
    if(this.loginUserData===null){
      console.log("status111",this.loginUserData);
      this.route.navigateByUrl('home');
    }
    this.dashboard_icons();
  }
    dashboard_icons() {
       
    }
}
