import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DealerCategoryComponent } from './dealer-category.component';

describe('DealerCategoryComponent', () => {
  let component: DealerCategoryComponent;
  let fixture: ComponentFixture<DealerCategoryComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DealerCategoryComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DealerCategoryComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
