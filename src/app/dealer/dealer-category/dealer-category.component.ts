import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-dealer-category',
  templateUrl: './dealer-category.component.html',
  styleUrls: ['./dealer-category.component.scss']
})
export class DealerCategoryComponent implements OnInit {

  constructor(private router:Router) { }

  ngOnInit() {
    alert("Coming Soon!!!");
    this.router.navigateByUrl('/dashboard');
  }

}
