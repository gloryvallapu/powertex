import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { GlobalServiceService } from '../../global-service.service';
import { DataServiceService } from '../../data-service.service';
import { Options } from 'ng5-slider';
// import { ComponentCommunicationService } from '../component-communication.service';
import { NgxSmartModalService } from 'ngx-smart-modal';
import { NgxSpinnerService } from 'ngx-spinner';


declare var $: any;
@Component({
  selector: 'app-dealer-status',
  templateUrl: './dealer-status.component.html',
  styleUrls: ['./dealer-status.component.scss']
})
export class DealerStatusComponent implements OnInit {
  POhidden: boolean = true;
  DOhidden: boolean = false;
  GRNhidden: boolean = false;
  PENhidden: boolean;
  sub: any;
  page: any = 1;
  page1: any = 1;
  orders: any;
  loginUserData: any;
  token: any;
  alert: boolean;
  obj: any = {};
  user_id: any;
  icon: boolean;
  panelOpenState = false;
  userid_cc: any;
  grn_data: any;
  constructor(private router: Router, private route: ActivatedRoute, private service: GlobalServiceService, public dataService: DataServiceService,
    private ngxSmartService: NgxSmartModalService, private spinner: NgxSpinnerService) {
    this.obj.id = 4;
  }

  ngOnInit() {
    this.alert = false;
    this.token = localStorage.getItem('token');
    this.loginUserData = JSON.parse(localStorage.getItem('loginUserData'));
    if(this.loginUserData===null){
      this.router.navigateByUrl('home');
    }
    this.sub = this.route.params.subscribe(params => {
      if (this.token == null) {
        this.user_id = '';
      } else {
        this.user_id = this.loginUserData.user_id;
        this.userid_cc=this.user_id+"@"+this.loginUserData.company_code;
      }
    });
    this.getOrders();
     this.getGrn();
  }
  getGrn(): any {
    console.log("dnm",this.userid_cc);
    let param1="";
    let param2="";
  
    this.service.getDatawithQueryParams3("8.0", param1, param2, this.userid_cc).subscribe((resp: Response) => {
    console.log(resp);
    
    this.grn_data=resp;
     
    });
  }

  getOrders() {
      this.spinner.show();
      this.service.getDatawithQueryParams1(7.6, this.user_id).subscribe((resp: Response) => {
        this.spinner.hide();
      this.getorddata(resp);
    });
  }

  getorddata(resp) {
    this.orders = resp.data;
  }

  po() {
    this.POhidden = true;
    this.DOhidden = false;
    this.GRNhidden = false;
  }

  do() {
    this.POhidden = false;
    this.DOhidden = true;
    this.GRNhidden = false;
  }

  grn() {
    this.POhidden = false;
    this.DOhidden = false;
    this.GRNhidden = true;
  }

  check_status(p) {
    for (let pd of p) {
      if (pd.status !== "Delivered") {
        return true;
      }
      return false;
    }
  }

  check_status1(p) {
    for (let pd of p) {
      if (pd.status == "Delivered") {
        return true;
      }
      return false;
    }
  }
}
