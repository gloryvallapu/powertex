import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { GlobalServiceService } from '../../global-service.service';
import { formatDate } from '@angular/common';
import { NgxSmartModalService } from 'ngx-smart-modal';
import { DataServiceService } from '../../data-service.service';
import { NgxSpinnerService } from 'ngx-spinner';
declare var $: any;

@Component({
  selector: 'app-goods-receipt-detail',
  templateUrl: './goods-receipt-detail.component.html',
  styleUrls: ['./goods-receipt-detail.component.scss']
})
export class GoodsReceiptDetailComponent implements OnInit {

  sub: any;
  invoice_num: any;
  invoice_date: any;
  packing_date: any;
  company_code: any;
  grn_details: any;
  showbtn: boolean = true;
  grndata: any = {};
  todayDate = new Date();
  headerData: { "document_no": number; "document_date": string; "creditperiod": number; "source_user_id": number; "grn_type": string; "dest_user_id": number; "ship_to_company_code": string; "address1": string; "address2": string; "address3": string; "address4": string; "city": string; "inv_origin_company_code": string; "state": string; "paymentterms": number; "remarks1": string; "remarks2": string; "splinstr": string; "currency_code": string; "exchange_rate": number; };
  grnList: any = [];
  loginUserData: any;
  stockDataList: any = [];
  grnprint: any;
  temp: any = 0;
  Sum: any = 0;
  qty: any;
  grnGetData: any;
  shipingAddress: any = {};
  billingAddress: any = {};

  constructor(private dat_s: DataServiceService, private router: Router, private route: ActivatedRoute, private globalService: GlobalServiceService, private ngxSmartService: NgxSmartModalService, private spinner: NgxSpinnerService) {
  }

  ngOnInit() {
    this.loginUserData = JSON.parse(localStorage.getItem('loginUserData'));
    if(this.loginUserData===null){
      this.router.navigateByUrl('home');
    }
    this.grnGetData = JSON.parse(localStorage.getItem('grnData'));
    console.log(this.grnGetData);
    this.getAddresses();
    this.sub = this.route.params.subscribe(params => {
      let d = params['invoice_no'];
      let e = params['invoice_date'];
      let f = params['company_code'];
      let g = params['packing_date'];
      this.invoice_num = atob(d);
      this.invoice_date = atob(e);
      this.company_code = atob(f);
      this.packing_date = atob(g);
      this.getData();
    },
      error => {
        this.ngxSmartService.getModal('errorModal').open();
        // console.log(error);
      });
  };

  getAddresses() {
    this.spinner.show();
    let param1 = this.grnGetData.bill_to_party_seq_no + "," + this.grnGetData.ship_to_party_seq_no;
    this.globalService.getDatawithQueryParams1(7.8, param1).subscribe((data) => {
      this.spinner.hide();
      localStorage.setItem('addresses', JSON.stringify(data));
      this.billingAddress = data.Bill;
      this.shipingAddress = data.SHIP;
    },
      error => {
        this.ngxSmartService.getModal('errorModal').open();
        // console.log(error);
      });
  };

  getData() {
    this.spinner.show();
    return this.globalService.getDatawithQueryParams3('3.9', '23', this.invoice_num, this.invoice_date).subscribe((resp: Response) => {
      this.spinner.hide();
      this.grn_details = resp; console.log("Details-GRN Detail", this.grn_details);
    //  localStorage.setItem('grnPrintData', JSON.stringify(resp));
      this.getVal();
    },
      error => {
        this.ngxSmartService.getModal('errorModal').open();
        // console.log(error);
      });
  }

  getVal() {
    for (let i of this.grn_details) {
      let qty = i.packing_qty;
      this.temp = this.temp + qty;
      let val = i.tot_value;
      this.Sum = this.Sum + val;
    }
  }

  Generate_GRN(data, grndata) {
    this.spinner.show();
    this.grndata.data = data;
    console.log(this.grndata.data.ship_to_company_code);
    this.headerData = {
      "document_no": 1,
      "document_date": formatDate(this.todayDate, 'yyyyMMdd', 'en-US'),
      "creditperiod": grndata.creditperiod,
      "source_user_id": 7,
      "grn_type": "DOM",
      "dest_user_id": 1,
      "ship_to_company_code": data[0].ship_to_company_code,
      "address1": "testst",
      "address2": "tesst",
      "address3": "",
      "address4": "",
      "city": "Dantewada",
      "inv_origin_company_code": data[0].inv_origin_company_code,
      "state": "Chhattisgarh",
      "paymentterms": grndata.paymentterms,
      "remarks1": grndata.remarks1,
      "remarks2": grndata.remarks2,
      "splinstr": grndata.splinstr,
      "currency_code": "INR",
      "exchange_rate": 0,
    }
    for (let i = 0; i < this.grn_details.length; i++) {
      console.log(this.grn_details[i].po_srl_no)
      //   const element = array[index];
      let json_dtl = {
        "document_no": 0,
        "document_date": formatDate(this.todayDate, 'yyyyMMdd', 'en-US'),
        "srl_no": this.grn_details[i].po_srl_no,
        "financial_year": this.grn_details[i].financial_year,
        "inv_date": this.grn_details[i].inv_date,
        "inv_no": this.grn_details[i].inv_no,
        "inv_qty": this.grn_details[i].inv_qty,
        "inv_srl_no": this.grn_details[i].inv_srl_no,
        "inv_type": "PO",
        "po_no": this.grn_details[i].po_no,
        "po_date": "20181127",
        "po_srl_no": this.grn_details[i].po_srl_no,
        "productid": this.grn_details[i].productid,
        "modelno": this.grn_details[i].modelno,
        "Material_recd_at_customer": this.grn_details[i].Material_recd_at_customer,
        "barcode_qty": 0,                          /* this.grn_details[i].barcode_qty*/
        "barcode_ship_pos_conf": 0,               /* this.grn_details[i].barcode_ship_pos_conf*/
        "grn_qty": 175,
        "mrp": this.grn_details[i].mrp,
        "net_price": this.grn_details[i].net_price,
        "tot_value": this.grn_details[i].tot_value,
        "discount1": this.grn_details[i].discount1,
        "discount2": this.grn_details[i].discount2,
        "discount3": this.grn_details[i].discount3,
        "discount_eff": this.grn_details[i].discount_eff
      }
      this.grnList.push(json_dtl);
    }
    let body = {
      "process_in": 'GRN', "operation_in": "INSERT", "draft_final_in": "FINAL", "document_no_out": "", "message_out": "",
      "json_hdr": this.headerData, "json_dtl": this.grnList
    }
    let methodName = "insert_update/"
    this.globalService.postData(body, methodName).subscribe((data) => {
      this.spinner.hide();
      console.log(data);
      this.stockUpdate();
      this.grnprint = data;
      console.log(this.grnprint);
      localStorage.setItem('grnprintData', JSON.stringify(data));
      // this.dat_s.setgrndata(this.grnprint);
      if (data.Message == "GRN Sucessfully inserted ") {
        $('#grnSuccessfulModal').modal('show');
      }
    },
      error => {
        this.ngxSmartService.getModal('errorModal').open();
        // console.log(error);
      });
  }

  confirm_grn() {
    this.router.navigateByUrl('/grn-print');
  };

  stockUpdate() {
    this.spinner.show();
    this.grn_details.forEach(data => {
      let stockData = {
        "productid": data.productid,
        "qty_in": data.inv_qty,
        "tot_amt_in": data.tot_value
      }
      this.stockDataList.push(stockData);
    });
    let body = {
      "period_in": formatDate(this.todayDate, 'yyyyMM', 'en-US'),
      "company_code_in": this.headerData.inv_origin_company_code,
      "product_details": this.stockDataList,
      "storage_location_in": "WAREHOUSE1",
      "process_type_in": "GRN"
    };
    console.log(body);
    let methodName = "proc_stock/"
    this.globalService.postData(body, methodName).subscribe((data) => {
      this.spinner.hide();
      console.log(data);
    },
      error => {
        this.ngxSmartService.getModal('errorModal').open();
        //console.log(error);
      });
  };

}
