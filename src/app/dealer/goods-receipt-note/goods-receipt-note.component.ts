import { Component, OnInit } from '@angular/core';
import { GlobalServiceService } from '../../global-service.service';
import { Router } from '@angular/router';
import { formatDate } from '@angular/common';
import { NgxSmartModalService } from 'ngx-smart-modal';
import { NgxSpinnerService } from 'ngx-spinner';

@Component({
  selector: 'app-goods-receipt-note',
  templateUrl: './goods-receipt-note.component.html',
  styleUrls: ['./goods-receipt-note.component.scss']
})
export class GoodsReceiptNoteComponent implements OnInit {
  grn_details: any = [];
  inw_details: any;
  searchText: any;
  grnheader: string;
  loginUserData: any;
  p: any = 1;
  constructor(private service: GlobalServiceService, private router: Router, private ngxSmartService: NgxSmartModalService, private spinner: NgxSpinnerService) { }

  ngOnInit() {
    this.loginUserData = JSON.parse(localStorage.getItem('loginUserData'));
    if(this.loginUserData===null){
      this.router.navigateByUrl('home');
    }
    console.log(this.loginUserData);
    this.getInward();
  }

  getInward() {
    this.spinner.show();
    let source_company_code = this.loginUserData.user_id + "@" + this.loginUserData.company_code;
    return this.service.getDatawithQueryParams1nd4('3.9', '24', source_company_code).subscribe((resp: Response) => {
      this.spinner.hide();
      console.log(resp);
      this.inw_details = resp; console.log("Details", this.inw_details);
    },
      error => {
        this.ngxSmartService.getModal('errorModal').open();
        // console.log(error);
      });
  }

  temp: any = 0;
  Sum: any = 0;
  qty: any;

  getVal() {
    for (let i of this.grn_details) {
      let qty = i.packing_qty;
      this.temp = this.temp + qty;
      console.log(this.temp);
      let val = i.tot_value;
      this.Sum = this.Sum + val;
      console.log(this.Sum);
    }
  }

  invoice_data(data) {
    console.log(data.inv_date);
    localStorage.setItem('grnData', JSON.stringify(data));
    let Date = formatDate(data.inv_date, 'yyyyMMdd', 'en-US');
    console.log(Date);
    let invoice_no = btoa(data.inv_no);
    let invoice_date = btoa(data.inv_date);
    let company_code = btoa(data.ship_to_company_code);
    let packing_date = btoa(data.packing_l_date);

    console.log("invoice number", invoice_no, "invoice date", invoice_date, "company code", company_code, "packing data", packing_date);
    this.router.navigate(['/grn_detail', invoice_no, invoice_date, company_code, packing_date]);
    // let ii= atob(i);
    // let dd= atob(d);
    // let cc= atob(c);
    // let pp = atob(p);

    // console.log("invoice number",ii,"invoice date",dd,"company code",cc,"packing data",pp);
  }
}
