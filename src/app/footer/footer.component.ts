import { Component, OnInit, Input } from '@angular/core';
import { GlobalServiceService } from '../global-service.service';
import { NavigationExtras, Router } from '@angular/router';
import { NgxSmartModalService } from 'ngx-smart-modal';
import { NgxSpinnerService } from 'ngx-spinner';

@Component({
  selector: 'app-footer',
  templateUrl: './footer.component.html',
  styleUrls: ['./footer.component.scss']
})
export class FooterComponent implements OnInit {
  resources: any;

  constructor(private service: GlobalServiceService, private router: Router, private ngxSmartService: NgxSmartModalService, private spinner: NgxSpinnerService) { }
  ngOnInit() {
    this.getprodimg();
    // this.getData1();
  }
  getprodimg() {
   // this.spinner.show();
    return this.service.getDatawithInput_id('14').subscribe((resp: Response) => {
  //    this.spinner.hide();
      this.resources = resp;  //this.getLimitData();
    },
      error => {
        this.ngxSmartService.getModal('errorModal').open();
        // console.log(error);
      });
  }
  getLimitData() {
    for (let p of this.resources) {

    }
  }

  all_catg() {
    this.router.navigateByUrl('/all-Category');
  }

  catg_click(p) {
    console.log(p.Category);
    this.router.navigate(['/prod-category', p.Category])
  }
}
