import { Injectable } from '@angular/core';
import { Http, RequestOptions, Headers } from "@angular/http";
import 'rxjs/add/operator/map';
import { Observable } from 'rxjs/Rx';

@Injectable({
  providedIn: 'root'
})
export class GlobalServiceService {

  options: any;
  response: any;

  constructor(public http: Http) {
    const headers = new Headers();
    headers.append('Content-Type', 'application/json');
    this.options = new RequestOptions({ headers: headers });
  };

  // apiUrl = 'http://157.119.108.142:8002/get/';
  // posturl = 'http://157.119.108.142:8002/';

  apiUrl = 'http://192.168.20.65:8000/get/';
  posturl = 'http://192.168.20.65:8000/';


  updateData(body, methodName) {
    return this.http.put(this.posturl + methodName, body, this.options).map(res => res.json());
  };

  //DELETE Method (DELETE)
  deleteData(methodName) {
    return this.http.delete(this.posturl + methodName, this.options).map(res => res.json());
  };

  // POST Method
  postData(body, methodName) {
    return this.http.post(this.posturl + methodName, body, this.options).map(res => res.json());
  };

  postDataWithParams(methodName, process_in, json_hdr, json_dtl, operation_in, draft_final_in, document_no_out, message_out) {
    return this.http.post(this.posturl + methodName + '&process_in=' + process_in + '&json_hdr=' + json_hdr + '&json_dtl=' + json_dtl + '&operation_in=' + operation_in + '&draft_final_in=' + draft_final_in + '&document_no_out=' + document_no_out + '&message_out=' + message_out, this.options).map(res => res.json());
  };

  getDatawithQuery(methodName, param) {
    return this.http.get(this.posturl + methodName + '?num=' + param, this.options).map(res => res.json());
  }

  // GET Method Start
  getData(methodName) {
    return this.http.get(this.apiUrl + '/' + methodName + '/', this.options).map(res => res.json());

  };

  getHeaderDetails(input_id, user_id) {
    return this.http.get(this.apiUrl + '?input_id=' + input_id + '&user_id=' + user_id, this.options).map(res => res.json());
  }

  getDatawithInput_id(input_id) {
    return this.http.get(this.apiUrl + '?input_id=' + input_id, this.options).map(res => res.json());
  };

  getDatawithQueryParams1(input_id, param1) {
    return this.http.get(this.apiUrl + '?input_id=' + input_id + '&param_other1=' + param1, this.options).map(res => res.json());
  };

  getDatawithQueryParams2(input_id, param1, param2) {
    return this.http.get(this.apiUrl + '?input_id=' + input_id + '&param_other1=' + param1 + '&param_other2=' + param2, this.options).map(res => res.json());
  };


  getDatawithQueryParams3(input_id, param1, param2, param3) {
    return this.http.get(this.apiUrl + '?input_id=' + input_id + '&param_other1=' + param1 + '&param_other2=' + param2 + '&param_other3=' + param3, this.options).map(res => res.json());
  };

  getDatawithQueryParams4(input_id, param1, param2, param3, param4) {
    return this.http.get(this.apiUrl + '?input_id=' + input_id + '&param_other1=' + param1 + '&param_other2=' + param2 + '&param_other3=' + param3 + '&param_other4=' + param4, this.options).map(res => res.json());
  };

  getDatawithQueryParams5(input_id, param1, param2, param3, param4, param5) {
    return this.http.get(this.apiUrl + '?input_id=' + input_id + '&param_other1=' + param1 + '&param_other2=' + param2 + '&param_other3=' + param3 + '&param_other4=' + param4 + '&param_other5=' + param5, this.options).map(res => res.json());
  };

  getDatawithQueryParams1nd4(input_id, param1, param4) {
    return this.http.get(this.apiUrl + '?input_id=' + input_id + '&param_other1=' + param1 + '&param_other4=' + param4, this.options).map(res => res.json());
  };

  getDatawithQueryParams6(input_id, param1, param2, param3, param4, param5, param6) {
    return this.http.get(this.apiUrl + '?input_id=' + input_id + '&param_other1=' + param1 + '&param_other2=' + param2 + '&param_other3=' + param3 + '&param_other4=' + param4 + '&param_other5=' + param5 + '&param_other6=' + param6, this.options).map(res => res.json());
  };
  getDatawithQueryParams7(input_id, param1, param2, param3, param4, param5, param6, param7) {
    return this.http.get(this.apiUrl + '?input_id=' + input_id + '&param_other1=' + param1 + '&param_other2=' + param2 + '&param_other3=' + param3 + '&param_other4=' + param4 + '&param_other5=' + param5 + '&param_other6=' + param6 + '&param_other7=' + param7, this.options).map(res => res.json());
  };
  getDatawithQueryParams4User_id(input_id, param1, param2, param3, param4, userid) {
    return this.http.get(this.apiUrl + '?input_id=' + input_id + '&param_other1=' + param1 + '&param_other2=' + param2 + '&param_other3=' + param3 + '&param_other4=' + param4 + '&user_id=' + userid, this.options).map(res => res.json());
  };

  getDatawithQueryParams7User_id(input_id, param1, param2, param3, param4, param5, param6, param7, userid) {
    return this.http.get(this.apiUrl + '?input_id=' + input_id + '&param_other1=' + param1 + '&param_other2=' + param2 + '&param_other3=' + param3 + '&param_other4=' + param4 + '&param_other5=' + param5 + '&param_other6=' + param6 + '&param_other7=' + param7 + '&user_id=' + userid, this.options).map(res => res.json());
  };

  //FORK METHODS

  forkJoinMethodForInputID1(input_id1, input_id2) {
    return Observable.forkJoin(
      this.http.get(this.apiUrl + '?input_id=' + input_id1).map((res: any) => res.json()),
      this.http.get(this.apiUrl + '?input_id=' + input_id2).map((res: any) => res.json()))
  }

}
