import { Component, OnInit, Input } from '@angular/core';
import { Router } from "@angular/router";
import { NgxSmartModalService } from 'ngx-smart-modal';
import { GlobalServiceService } from '../global-service.service';
import { DataServiceService } from '../data-service.service';
import { ComponentCommunicationService } from '.././component-communication.service';
import { NgxSpinnerService } from 'ngx-spinner';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})
export class HeaderComponent implements OnInit {
  loginUserData: any;
  cartItem_count: any;
  search_val: string;
  token: any;
  resources1: Response;
  wishList_count: any;
  navbarCollapsed:any;
  //  @Input() loginUserData:any;
  constructor(private route: Router, public ngxSmartService: NgxSmartModalService, public globalservive: GlobalServiceService,
    public dataservice: DataServiceService, private eventOn: ComponentCommunicationService, private spinner: NgxSpinnerService) {
  }

  ngOnInit() {
    //location.reload();
    this.token = localStorage.getItem('token');
    this.loginUserData = JSON.parse(localStorage.getItem('loginUserData'));
    if (this.token != undefined && (this.loginUserData.user_type == 'Customer' || this.loginUserData.user_type == 'Dealer' || this.loginUserData.user_type == 'Guest')) {
      this.ItemsCount();
    }
    this.eventOn.on().subscribe(data => {
      if (data.id == 1) { //cart Updation
        this.cartItem_count = data.cartItem_count;
      } else if (data.id == 2) { //logout
        this.token = "";
        this.loginUserData = "";
        localStorage.clear();
      } else if (data.id == 3) { //Profile Updation
        this.loginUserData = data.loginUserData;
      } else if (data.id == 4) { //WishList Updation
        this.wishList_count = data.wishList_count;
        console.log(this.wishList_count);
      }

    },
      error => {
        this.ngxSmartService.getModal('errorModal').open();
        // console.log(error);
      });


  }

  openLoginModal() {
    this.ngxSmartService.getModal('loginModal').open();
  };

  openLoginModalAlert() {
    this.ngxSmartService.getModal('alertModal').open();
  }

  viewcart() {
    //this.route.navigateByUrl('/viewcart');
  }

  ItemsCount() {
    this.spinner.show();
    this.globalservive.getDatawithQueryParams1('4.4', this.loginUserData.user_id).subscribe((data) => {
      this.spinner.hide();
      this.cartItem_count = data.cartcount;
      this.wishList_count = data.wishlist_count;
    },
      error => {
        this.ngxSmartService.getModal('errorModal').open();
        // console.log(error);
      });
  }

  gotoLogout() {
    this.spinner.show();
    var methodname = "logout/"
    var body = { "token": this.token }
    this.globalservive.postData(body, methodname).subscribe((data) => {
      this.spinner.hide();
      console.log(data);
      this.token = "";
      this.loginUserData = "";
      localStorage.clear();
    },
      error => {
        this.ngxSmartService.getModal('errorModal').open();
        // console.log(error);
      });
  }

  search_btn(inp) {

    console.log(inp);

    console.log(this.search_val);
    this.route.navigate(['/search', this.search_val]);
    // return this.globalservive.getDatawithQueryParams1('7.3',inp).subscribe((resp: Response) => { console.log(resp);
    //     this.resources1 = resp;  })

  }

  search_disable() {

    if (undefined !== this.search_val && this.search_val.length >= 2) {

      console.log(this.search_val.length);
      return false;

    } else {

      return true;


    }

  }
}
