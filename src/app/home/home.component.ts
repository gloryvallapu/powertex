import { Component, OnInit } from '@angular/core';
import { GlobalServiceService } from '../global-service.service';
import { Router } from '@angular/router';
import { NgxSmartModalService } from 'ngx-smart-modal';
import { NgxSpinnerService } from 'ngx-spinner';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {
  resources: any = [];
  resources1: any;
  l:number=11;
  prof: any;

  constructor(private service: GlobalServiceService, private router: Router, private ngxSmartService: NgxSmartModalService, private spinner: NgxSpinnerService) { }

  ngOnInit() {

    // this.getproddata();
    this.getprodimg();
    this.get_prof();
  }

  images:any=["slide1","slide2" ,"slide3","slide4","slide5","slide6","slide7","slide8","slide9","slide10","slide11","slide12"];
  
  get_prof() {
    this.spinner.show();
    return this.service.getDatawithInput_id('61').subscribe((resp: Response) => {
      this.spinner.hide();
      this.prof = resp;
      console.log(this.prof);

    },
      error => {
        this.ngxSmartService.getModal('errorModal').open();
        // console.log(error);
      });
  }
  getprodimg() {
    this.spinner.show();
    return this.service.getDatawithInput_id('14').subscribe((resp: Response) => {
      this.spinner.hide();
      this.resources = resp;
      console.log(this.resources);

    },
      error => {
        this.ngxSmartService.getModal('errorModal').open();
      });
  }
 
  catg_prod(data) {
    this.spinner.show();
    // console.log(data);
    let category = data.Category;
    
    return this.service.getDatawithQueryParams1('4.8', category).subscribe((resp: Response) => {
      this.spinner.hide();
      this.resources1 = resp; this.getprod_deatils();
    })
  }
  getprod_deatils() {
    for (let d of this.resources1) {
      let sub_c = d.subcategory;
    }
  }
  selected_catg(cat) {
    let category = cat;
    this.router.navigate(['/prod-category', category])

  }
  selected_all() {
    this.router.navigateByUrl('/all-Category');
  }
}
