import { Component, OnInit, ViewChild, OnDestroy } from '@angular/core';
import { Router } from "@angular/router";
import { GlobalServiceService } from "../global-service.service";
import { DataServiceService } from "../data-service.service";
import { NgxSmartModalService } from "ngx-smart-modal";
import { HeaderComponent } from '../header/header.component';
import { Observable } from 'rxjs';

declare var $: any;

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {
  body: any;
  loginMoethod: string;
  loginUserData: any;
  headeDetails: any;
  today = new Date();
  token: any;
  @ViewChild(HeaderComponent) HeaderComponent;
  checkUser: any = {};
  otpData: any = {};
  message: any;

  showLogin: any = true;
  showForgotPswd: any = false;
  showResetPswd: any = false;
  cartItem_count: any;
  counter: any;
  showResendbtn: boolean;
  otpTimer: any = 60;
  guestId: any;
  wish_alert: any;
  alert: boolean;
  icon: boolean;
  icon2: boolean;


  constructor(private authService: GlobalServiceService, private ngxSmartService: NgxSmartModalService, private route: Router,
    private dataService: DataServiceService, private headerComponent: HeaderComponent) {
  }

  loginData: any = {};
  headerData: any = {};
  date: any;
  ngOnInit() {
    this.alert = false;
  };

  timer() {
    this.counter = setInterval(() => {
      if (this.otpTimer > 0) {
        this.otpTimer--;
      } else {
        this.otpTimer = 60;
        this.showResendbtn = true;
        clearInterval(this.counter);
      }
    }, 1000)
  };


  closeModal() {
    this.loginData = {};
    this.showForgotPswd = false;
    this.showLogin = true;
    this.showResetPswd = false;
    this.checkUser = {};
    this.showResendbtn = false;
  }

  gotoLogin() {
    this.loginMoethod = 'login/';
    this.body = { "username": this.loginData.userId, "password": this.loginData.password };
    this.authService.postData(this.body, this.loginMoethod).subscribe((data) => {
      if (data.status == "success") {
        this.message = data.status;
        this.token = data.token;
        localStorage.setItem('token', this.token);
        this.loginUserData = data.data;
        this.ngxSmartService.close('loginModal');
        localStorage.setItem('loginUserData', JSON.stringify(this.loginUserData));
        if (this.loginUserData.user_type != 'Customer' && this.loginUserData.user_type != 'Guest') {
          this.route.navigateByUrl("dashboard");
        } else {
          this.route.navigateByUrl("home");
        }
        this.headerComponent.token = data.token;
        this.headerComponent.loginUserData = data.data;
        if (this.loginUserData.user_type == 'Customer' || this.loginUserData.user_type == 'Dealer' || this.loginUserData.user_type == 'Guest') {
          this.ItemsCount();
        }
      } else {
        alert(data.status);
      }
    },
      error => {
        this.ngxSmartService.getModal('errorModal').open();
        // console.log(error);
      });

  };

  ItemsCount() {
    this.authService.getDatawithQueryParams1('4.4', this.loginUserData.user_id).subscribe((data) => {
      this.cartItem_count = data.cartcount;
      this.headerComponent.cartItem_count = data.cartcount;
    },
      error => {
        this.ngxSmartService.getModal('errorModal').open();
        // console.log(error);
      });
  }

  gotoRegister() {
    this.ngxSmartService.close('loginModal');
    this.route.navigate(['/registration', 1]);
  };

  gotoForgotPassword(id) {
    this.guestId = id;
    this.showLogin = false;
    this.showForgotPswd = true;
  };


  resendPassword() {
    this.getOTP();
  }

  getOTP() {
    //Check User Existed or Not
    this.authService.getDatawithQueryParams1(4.9, this.loginData.userId).subscribe((data) => {
      this.checkUser = data;
      // if (data.status == "1") {
      this.callOTP();
      // }
    },
      error => {
        this.ngxSmartService.getModal('errorModal').open();
        // console.log(error);
      });
  };

  callOTP() {
    var regMoethod = 'sendotp/';
    this.authService.getDatawithQuery(regMoethod, this.loginData.userId).subscribe((data) => {
      this.otpData = data;
      this.showResendbtn = false;
      this.timer();
    },
      error => {
        this.ngxSmartService.getModal('errorModal').open();
        // console.log(error);
      });
  };



  verifyOTP(guestId) {
    var regMoethod = 'sendotp/';
    var otpValidBody = { "otp": this.loginData.otp, "phone": this.loginData.userId };
    this.authService.postData(otpValidBody, regMoethod).subscribe((data) => {
      this.otpData = data;
      if (data.status == 'otp verify !!!') {
        if (guestId == '2') {
          this.message = data.status;
          this.showResetPswd = true;
          this.showForgotPswd = false;
        } else if (guestId == '1') {
          this.loginAsGuest();
        }

      } else {
        alert(data.status);
      }
    },
      error => {
        this.ngxSmartService.getModal('errorModal').open();
        // console.log(error);
      });
  };

  resetPassword() {
    if (this.loginData.setpassword == this.loginData.confirmPassword) {
      var regMoethod = 'reset_password/';
      var resetPswdBody = { "username": this.loginData.userId, "password": this.loginData.setpassword };
      this.authService.postData(resetPswdBody, regMoethod).subscribe((data) => {
        if (data.status == 1) {
          // $("#loginResetPwdModal").modal('show');
          this.wish_alert = "Password Updated Successfully"
          this.addwish();
          this.icon = true;
          this.icon2 = false;
          this.loginData = {};
          this.checkUser = {};
          this.showForgotPswd = false;
          this.showLogin = true;
          this.showResetPswd = false;
          this.showResendbtn = false;
        } else {
          //alert('your password should not be similar to old password');
          this.wish_alert = "your password should not be similar to old password"
          this.addwish();
          this.icon = false;
          this.icon2 = true;
        }
      },
        error => {
          this.ngxSmartService.getModal('errorModal').open();
          // console.log(error);
        });
    } else {
      this.wish_alert = "Password and Confirm Password should match"
      this.addwish();
      this.icon = false;
      this.icon2 = true;
      //alert("password Should match");
    }
  };

  addwish() {
    this.alert = true;
    setInterval(() => {
      this.alert = false;
    }, 5000);
  }

  loginAsGuest() {
    let regMoethod = 'guest_user/';
    let resetPswdBody = { "username": this.loginData.userId, "password": this.loginData.setpassword };
    this.authService.postData(resetPswdBody, regMoethod).subscribe((data) => {
      if (data.Status == 'success') {
        this.token = data.token;
        console.log(data);
        localStorage.setItem('token', this.token);
        this.loginUserData = data.data;
        this.ngxSmartService.close('loginModal');
        localStorage.setItem('loginUserData', JSON.stringify(this.loginUserData));
        this.route.navigateByUrl("home");
        this.headerComponent.token = data.token;
        this.headerComponent.loginUserData = data.data;
      } else {
        alert(data.Status);
      }

    },
      error => {
        this.ngxSmartService.getModal('errorModal').open();
        // console.log(error);
      });
  }

}
