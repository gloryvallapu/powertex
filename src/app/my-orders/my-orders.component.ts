import { Component, OnInit } from '@angular/core';
import { GlobalServiceService } from "../global-service.service";
import { NgxSmartModalService } from 'ngx-smart-modal';
import { NgxSpinnerService } from 'ngx-spinner';
declare var $: any;

@Component({
  selector: 'app-my-orders',
  templateUrl: './my-orders.component.html',
  styleUrls: ['./my-orders.component.scss']
})
export class MyOrdersComponent implements OnInit {
  loginUserData: any;
  myOrders: any = [];
  orderdetail: any = [];
  today: Date = new Date();
  wish_alert: any;
  icon: boolean;
  alert: boolean;
  P:any =1;
  searchText: any;
  Page: any = 1;
  review:any={};
  serial_no: any;
  pid: any;
  rating: any;
  constructor(private myOrderService: GlobalServiceService, private ngxSmartService: NgxSmartModalService, private spinner: NgxSpinnerService) { }

  ngOnInit() {
    this.loginUserData = JSON.parse(localStorage.getItem('loginUserData'));
    console.log(this.loginUserData);
    this.myOrdersget();
  }

  myOrdersget() {
    this.spinner.show();
    this.myOrderService.getDatawithQueryParams1(7.6, this.loginUserData.user_id).subscribe((data) => {
      this.spinner.hide();
      console.log(data);
      if (data.status == 1) {
        this.myOrders = data.data;
        console.log(this.myOrders);
      }
      else if (data.status == 0) {
        //alert("No data is available.")
        // this.wish_alert = "No data is available."
        // this.addwish();
      }
      else {

      }
    },
      error => {
        this.ngxSmartService.getModal('errorModal').open();
        // console.log(error);
      });
  }

  // addwish() {
  //   this.alert = true;
  //   setInterval(() => {
  //     this.alert = false;
  //   }, 5000);
  // }
  feedback(order){
   console.log("tt",order.seqno);
   this.serial_no=order.seqno;
   this.pid=order.productid;
   this.rating=order.rating;
   
    $('#reviewfulModal').modal('show');
  }
  submit_review(rev){
    this.spinner.show();
    console.log(rev);
    let loginMethod = 'review/';
    let body = {"sno": this.serial_no, "userid": this.loginUserData.user_id,"productid":this.pid,"comments":rev.comment,"title":rev.title,"rating":this.rating, "permission":0 };
     return this.myOrderService.postData(body, loginMethod).subscribe((data) => {
      this.spinner.hide();
      if (data.status == "inserted" || "updated") {
        console.log(data.status);
        
      }
    });
  }
  get_detail(order){
   console.log(order);
   
  }
}
