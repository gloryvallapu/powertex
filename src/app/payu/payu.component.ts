import { Component, OnInit } from '@angular/core';
import { SHA512 } from 'crypto-js';
import { GlobalServiceService } from "../global-service.service";

@Component({
  selector: 'app-payu',
  templateUrl: './payu.component.html',
  styleUrls: ['./payu.component.scss']
})
export class PayuComponent implements OnInit {
  hash: any;
  txnid: any;
  hash1: any;
  showData: boolean = false;
  browser: any;
  currentURL: any = window.location.href;
  test: any = {
    'amount': 1,
    'txnid': '49fadefdf2774f1e9f73190b0979a956',
    'key': 'sKRvVl',
    'salt': 'rHlRMHhl',
    'productinfo': 'test',
    'firstname': 'Neelima',
    'email': 'test@gmail.com',
    'phone': 9676649989,
  }


  constructor(private globalService: GlobalServiceService) { }

  ngOnInit() {
    let stringData = this.test.key + '|' + this.test.txnid + '|' + this.test.amount + '|' + this.test.productinfo + '|' + this.test.firstname + '|' + this.test.email + '|||||||||||' + this.test.salt;
    console.log(stringData);

    this.hash = SHA512(stringData).toString();
    console.log(this.hash);
    alert(this.currentURL);

   
  }

}

