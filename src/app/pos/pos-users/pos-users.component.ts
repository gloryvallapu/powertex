import { Component, OnInit } from '@angular/core';
import { GlobalServiceService } from '../../global-service.service';
import { NgxSmartModalService } from 'ngx-smart-modal';
import { Router } from '@angular/router';
import { formatDate } from "@angular/common";
declare var $: any;
import { NgxSpinnerService } from 'ngx-spinner';

@Component({
  selector: 'app-pos-users',
  templateUrl: './pos-users.component.html',
  styleUrls: ['./pos-users.component.scss']
})
export class PosUsersComponent implements OnInit {
  isModal: boolean = false;
  httpdata2: any = [];
  httpdata3: any = [];
  mobile: any = "";
  httpdata4: any = [];
  bar_code: any = "";
  httpdata5: any = [];
  posModel: any = {};
  p: any = 1;
  todayDate = new Date();
  loginUserData: any;
  showMobile: boolean;
  showBarcode: boolean;
  invoiceListData: any = [];
  headerData: any = {};
  invoiceList: any[];
  keys: string[];
  newUserMsg: any = false;
  stockDataList: any;
  totalValue: any;

  constructor(private service: GlobalServiceService, private ngxSmartService: NgxSmartModalService, private router: Router, private spinner: NgxSpinnerService) { }
  ngOnInit() {
    this.loginUserData = JSON.parse(localStorage.getItem('loginUserData'));
    console.log(this.loginUserData);

    if (this.loginUserData === null) {
      this.router.navigateByUrl('home');
    }
    this.getuserdata();
    this.barcode();
  }
  open() {
    this.isModal = true;
  }

  pro(ev) {
    this.isModal = ev;
  }

  getuserdata() {
    this.service.getDatawithQueryParams2('5.2', "", "POS").subscribe((resp: Response) => {
      this.httpdata2 = resp;

    },
      error => {
        this.ngxSmartService.getModal('errorModal').open();
      });
  };

  getMobile(mobile) {
    this.mobile = mobile;
    this.showMobile = false;
  };

  changeMobile() {
    this.showMobile = true;
  };
  changeBarCode() {
    this.showBarcode = true;
  };
  getBarCode(barCode) {
    this.bar_code = barCode;
    this.showBarcode = false;
  }

  fetch(p) {
    // this.spinner.show();
    console.log(p);
    this.service.getDatawithQueryParams3('5.2', "", "POS", p).subscribe((resp: Response) => {
      this.httpdata3 = resp;
      if (this.httpdata3.length == 0) {
        // this.isModal = true;
        this.newUserMsg = true;
      }
    },
      error => {
        this.ngxSmartService.getModal('errorModal').open();
      });
  };


  barcode() {
    //  this.spinner.show();
    return this.service.getDatawithInput_id('4.3').subscribe((resp: Response) => {
      this.httpdata4 = resp;
    },
      error => {
        this.ngxSmartService.getModal('errorModal').open();
      });
  };


  bar_code_fetch(bar_code) {
    this.service.getDatawithQueryParams1('4.6', bar_code).subscribe((resp: Response) => {
      this.bar_code = "";
      this.httpdata5 = resp;
      this.invoiceListData.push(this.httpdata5[0]);

    },
      error => {
        this.ngxSmartService.getModal('errorModal').open();
      });
  };


  getGrandTotalVal(item, val) {
    Object.defineProperty(item, "totalVal", { value: val });
    this.totalValue = this.invoiceListData.filter((item) => item.totalVal)
      .map((item) => +item.totalVal)
      .reduce((sum, current) => sum + current);
  }


  deleteItem(item, totalVal) {
    this.invoiceListData = this.invoiceListData.filter(order => order.productid !== item.productid);
    this.getGrandTotalVal(totalVal, 1);
  }

  makeInvoice() {

    this.headerData = {
      document_no: "",
      document_date: formatDate(this.todayDate, 'yyyyMMdd', 'en-US'),
      dest_company_code: this.mobile,
      source_company_code: this.loginUserData.company_code,
      inv_type: 'POS',
      shipment_point: 0,
      payment_terms: 0,
      currency_code: "",
      exchange_rate: 0,
      credit_period: 0,
      spl_instr: "",
      fin_year: "",
      bill_to_party_seq_no: 0,
      ship_to_party_seq_no: 0,
      created_user_id: this.loginUserData.user_id,
      pos_user_id: this.loginUserData.user_id
    }



    this.invoiceList = [];
    this.keys = Object.keys(this.posModel);
    for (let i = 0; i < this.invoiceListData.length; i++) {
      for (let j = 0; j < this.keys.length; j++) {
        if (this.invoiceListData[i].productid == this.keys[j]) {
          let json_dtl = {
            "document_no": i,
            "document_date": formatDate(this.todayDate, 'yyyyMMdd', 'en-US'),
            "discount_eff": this.invoiceListData[i].discount_eu1,
            "performainv_date": formatDate(this.todayDate, 'yyyyMMdd', 'en-US'),
            "po_srl_no": "",
            "po_date": formatDate(this.todayDate, 'yyyyMMdd', 'en-US'),
            "productid": this.invoiceListData[i].productid,
            "packing_qty": this.posModel[this.keys[j]],
            "net_price": JSON.parse(this.posModel[this.keys[j]]) * JSON.parse(this.invoiceListData[i].mrp) * (1 - (this.invoiceListData[i].discount_eu1 / 100)),
            "performa_inv_qty": 0,
            "mrp": this.invoiceListData[i].mrp,
            "tot_value": JSON.parse(this.posModel[this.keys[j]]) * JSON.parse(this.invoiceListData[i].mrp) * (1 - (this.invoiceListData[i].discount_eu1 / 100)) * (1 + this.invoiceListData[i].gst / 100),
            "srl_no": i + 1,
            "inv_srl_no": 0,
            "inv_qty": 0
          }
          this.invoiceList.push(json_dtl);
        }
      }
    }

    let body = {
      "process_in": 'INVPOS', "operation_in": "INSERT", "draft_final_in": "FINAL", "document_no_out": "", "message_out": "",
      "json_dtl": this.invoiceList, "json_hdr": this.headerData
    }
    console.log(body);
    let methodName = "insert_update/"
    this.spinner.show();
    this.service.postData(body, methodName).subscribe((data) => {
      console.log(data);
      this.spinner.hide();
      if (data.Message == "POS Invoice  Sucessfully inserted") {
        $('#succModal').modal('show');

        //  this.stockUpdate();
      }
    },
      error => {
        this.spinner.hide();
        this.ngxSmartService.getModal('errorModal').open();
      });
  };

  gotoPrevious() {
    this.newUserMsg = false;
    this.mobile = "";
    this.invoiceListData = [];
  };

  stockUpdate() {
    this.invoiceList.forEach(data => {
      let stockData = {
        "productid": data.productid,
        "qty_in": data.packing_qty,
        "tot_amt_in": data.tot_value
      }
      this.stockDataList.push(stockData);
    });
    let body = {
      "period_in": formatDate(this.todayDate, 'yyyyMM', 'en-US'),
      "company_code_in": this.loginUserData.company_code,
      "product_details": this.stockDataList,
      "storage_location_in": "POS",
      "process_type_in": "POS"
    };
    console.log(body);
    let methodName = "proc_stock/"
    this.service.postData(body, methodName).subscribe((data) => {
      console.log(data);
    },
      error => {
        this.ngxSmartService.getModal('errorModal').open();
        //console.log(error);
      });

  };

}
