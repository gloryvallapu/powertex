import { Component, OnInit } from '@angular/core';
import { GlobalServiceService } from '../global-service.service';
import { Router, ActivatedRoute } from '@angular/router';
import { NgxSmartModalService } from 'ngx-smart-modal';
import { NgxSpinnerService } from 'ngx-spinner';

@Component({
  selector: 'app-prod-catgegory',
  templateUrl: './prod-catgegory.component.html',
  styleUrls: ['./prod-catgegory.component.scss']
})
export class ProdCatgegoryComponent implements OnInit {
  resources2: any;

  constructor(private service: GlobalServiceService, private router: Router, private route: ActivatedRoute, private ngxSmartService: NgxSmartModalService, private spinner: NgxSpinnerService) { }
  category: any;
  page: any = 1;
  ngOnInit() {
    let data = this.route.params.subscribe(params => {
      this.category = params['category'];
      console.log(this.category);
      this.getdata();
    },
    error => {
      this.ngxSmartService.getModal('errorModal').open();
      // console.log(error);
    });
  }
  modal: any = [1];
  getdata() {
    this.spinner.show();
    return this.service.getDatawithQueryParams1('4.8', this.category).subscribe((resp: Response) => {
      this.spinner.hide();
      resp;

      this.resources2 = resp;
    },
    error => {
      this.ngxSmartService.getModal('errorModal').open();
      // console.log(error);
    });
  }
  selected_sub_cat(p) {
    console.log(p);
    console.log(this.category);
    let b = this.category;
    let c = btoa(p);

    let d = btoa(this.modal);
    let e = btoa("All");
    this.router.navigate(['/category', b, c, d, e]);
  }


}
