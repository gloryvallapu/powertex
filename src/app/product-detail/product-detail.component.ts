import { Component, OnInit, ViewChild, Input, ChangeDetectorRef, ChangeDetectionStrategy } from '@angular/core';
import { GlobalServiceService } from '../global-service.service';
import { DataServiceService } from '../data-service.service';
import { ActivatedRoute, Router } from '@angular/router';
import { HeaderComponent } from '../header/header.component';
import { ComponentCommunicationService } from '.././component-communication.service';
import { NgxSmartModalService } from 'ngx-smart-modal';
import { Observable, interval } from 'rxjs';
import { startWith, take, map } from 'rxjs/operators';
import { NgxSpinnerService } from 'ngx-spinner';
import { slider } from './product.animation';
declare var $: any;

@Component({
  selector: 'app-product-detail',
  templateUrl: './product-detail.component.html',
  styleUrls: ['./product-detail.component.scss'],
  animations: [slider],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class ProductDetailComponent implements OnInit {
  methodname: string;
  input_id: string;
  user_id: any;
  x: any;
  sub: any;
  category: any;
  sub_c: any;
  model: any;
  select: any;
  details: any;
  cartItem_count: any;
  loginUserData: any;
  @ViewChild(HeaderComponent) HeaderComponent;
  token: any;
  obj: any = {};
  subcatg: any;
  attributes: any = [];
  message: any;
  prod_details: any = [];
  cr_category: any;
  modal:any=[1];
  review: any =[];
  constructor(public globalService: GlobalServiceService, public dataservice: DataServiceService, public route: ActivatedRoute, private router: Router,
    private eventemit: ComponentCommunicationService, private ngxSmartService: NgxSmartModalService, private cdr: ChangeDetectorRef, private spinner: NgxSpinnerService) {
    this.obj.id = 1;
  }
  data: any;
  ngOnInit() {
    this.token = localStorage.getItem('token');
    this.loginUserData = JSON.parse(localStorage.getItem('loginUserData'));
    console.log(this.loginUserData);
    this.sub = this.route.params.subscribe(params => {
      this.category = params['category'];
      this.sub_c = params['sub_category'];
      this.model = params['model'];
      this.select = "Select";
      console.log(this.category, "", this.sub_c, "", this.model);
      this.getData();

    },
      error => {
        this.ngxSmartService.getModal('errorModal').open();
      });

    this.data = JSON.parse(localStorage.getItem('key'));
  }

  getData() {
    //this.spinner.show();
    return this.globalService.getDatawithQueryParams4('10', this.category, this.sub_c, this.select, this.model).subscribe((resp: Response) => {
     // this.spinner.hide();
      console.log(resp);

      this.details = resp; console.log("details", this.details);
      this.sub_cat();
      this.getSimilar();
      this.getreviews();

    },
      error => {
        this.ngxSmartService.getModal('errorModal').open();
      });
  }
  getreviews() {
   this.spinner.show();
    for (let p of this.details) {
   console.log(p.productid);
   return this.globalService.getDatawithQueryParams1('8.0',p.productid ).subscribe((resp: Response) => {
    this.spinner.hide();
    console.log(resp);
    this.review = resp; console.log("rev", this.review);
  },
    error => {
      this.ngxSmartService.getModal('errorModal').open();
    });
    }
  }

  sub_cat() {

    for (let p of this.details) {
      this.subcatg = p.modelno;
      console.log("sub", this.subcatg);

    }
    return this.getattribute()
  }
  getattribute() {
    this.spinner.show();
    return this.globalService.getDatawithQueryParams1('3.8', this.subcatg).subscribe((resp: Response) => {
      this.spinner.hide();
      console.log("attr",resp);

      this.attributes = resp;

    },
      error => {
        this.ngxSmartService.getModal('errorModal').open();
        // console.log(error);
      });
  }


  addToCart(body) {
  //  this.spinner.show();
    if (this.token != '' && this.token != undefined) {
      this.methodname = "addtocart_site/";
      body = { "user_id": this.loginUserData.user_id, "productid": this.data.productid, "qty": 1 }
      this.globalService.postData(body, this.methodname).subscribe((data) => {
    //    this.spinner.hide();
        if (data.Status == "Update sucessfully") {
          this.obj.cartItem_count = data.count;
          this.eventemit.fire(this.obj);
          $('#alrdyItemAddedModal').modal('show');
        }
        else if (data.Status == "Inserted sucessfully") {
          this.obj.cartItem_count = data.count;
          this.eventemit.fire(this.obj);
          $('#insertItemModal').modal('show');
        }
      },
        error => {
          this.ngxSmartService.getModal('errorModal').open();
          // console.log(error);
        });
    } else {
      this.ngxSmartService.getModal('loginModal').open();
    }

  };
  gotoPreviousInsert() {
    this.router.navigateByUrl('/ product-detail');
  }

  gotoPrevious() {
    this.router.navigateByUrl('/viewcart');
  }


  buyNow(item) {
    console.log("dd",item);
    
    if (this.token != '' && this.token != undefined) {
      localStorage.setItem('buynowItem', JSON.stringify(item));

      this.router.navigateByUrl('checkout/2');
    } else {
      this.ngxSmartService.getModal('loginModal').open();
    }
  }
  getSimilar() {
   // this.spinner.show();
    let select = "All";
    return this.globalService.getDatawithQueryParams4('10', this.category, this.sub_c, select, this.model).subscribe((resp: Response) => {
    //  this.spinner.hide();
      console.log(resp);

      this.prod_details = resp; console.log("details", this.prod_details);

    },
      error => {
        this.ngxSmartService.getModal('errorModal').open();
        // console.log(error);
      });
  }
  //bread-crumbs
  catg_crumb(category) {
    this.cr_category=category;
    this.router.navigate(['/prod-category', category])
  }
  sub_catg_crumb(sub_c) {
    let b=this.category;
    let c=btoa(sub_c);
    let d= btoa(this.modal);
    let e= btoa("All")
       this.router.navigate(['/category', b,c,d,e])
  }
}
