import { Component, OnInit } from '@angular/core';
import { GlobalServiceService } from '../../global-service.service';
import { Router } from '@angular/router';
import { formatDate } from '@angular/common';
import { NgxSmartModalService } from 'ngx-smart-modal';
import { NgxSpinnerService } from 'ngx-spinner';

@Component({
  selector: 'app-import-grn',
  templateUrl: './import-grn.component.html',
  styleUrls: ['./import-grn.component.scss']
})
export class ImportGrnComponent implements OnInit {
  loginUserData: any;
  inw_details: Response;
  searchText: any;
  p: any = 1;


  constructor(private service: GlobalServiceService, private router: Router, private ngxSmartService: NgxSmartModalService, private spinner: NgxSpinnerService) { }

  ngOnInit() {
    this.loginUserData = JSON.parse(localStorage.getItem('loginUserData'));
    console.log(this.loginUserData);
    if (this.loginUserData === null) {
      this.router.navigateByUrl('home');
    }
    this.getGrnGeaderData()
  }
  getGrnGeaderData() {
    this.spinner.show();
    let source_company_code = this.loginUserData.user_id + "@" + this.loginUserData.company_code;
    return this.service.getDatawithQueryParams1nd4('3.9', '25', source_company_code).subscribe((resp: Response) => {
      this.spinner.hide();
      console.log(resp);

      this.inw_details = resp;
      console.log(this.inw_details);
    },
      error => {
        this.ngxSmartService.getModal('errorModal').open();
      });
  };
  getInvData(data) {
    localStorage.setItem('grnData', JSON.stringify(data));
    this.router.navigateByUrl('product-GRN');
  }


}
