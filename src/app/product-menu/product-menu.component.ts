import { Component, OnInit, Input } from '@angular/core';
import { GlobalServiceService } from '../global-service.service';
import { NavigationExtras ,Router} from '@angular/router';
import { NgxSmartModalService } from 'ngx-smart-modal';
import { NgxSpinnerService } from 'ngx-spinner';

@Component({
  selector: 'app-product-menu',
  templateUrl: './product-menu.component.html',
  styleUrls: ['./product-menu.component.scss']
})
export class ProductMenuComponent implements OnInit {
  resources:any=[];
  resources1:any=[];
  resources2:any=[];
  data1: Response;
  SelectedSubCategory: any;
  // isChecked:boolean=false;
    constructor( private service:GlobalServiceService,private router:Router, private ngxSmartService: NgxSmartModalService, private spinner: NgxSpinnerService) { }
    catgeory_prod:any;
    subcategory_prod:any;
    select_prod:any;
  
    ngOnInit() {
      this.getprodimg();
      // this.getData1();
    }
    getprodimg() {
      this.spinner.show();
      return this.service.getDatawithInput_id('14').subscribe((resp: Response) => {  
        this.spinner.hide();
        this.resources = resp;  
      },
      error => {
        this.ngxSmartService.getModal('errorModal').open();
        // console.log(error);
      });
    }
  
    product(a:any,r:any){
      // console.log(a,'',r);
      // console.log(this.modal);
      
       this.catgeory_prod=a;
       this.subcategory_prod=r;
       this.select_prod="Select";
       this.modal;
       let b=a;
       let c=btoa(r);
     let d= btoa(this.modal);
     let e= btoa("Select")
     
      this.router.navigate(['/category', b,c,d,e]);

      //  return this.getData();
    }
    getData(){
      this.spinner.show();
      return this.service.getDatawithQueryParams4('10',this.catgeory_prod,this.subcategory_prod,this.select_prod,this.modal).subscribe((resp: Response) => { 
        this.spinner.hide();
        this.resources1 = resp;  
      },
      error => {
        this.ngxSmartService.getModal('errorModal').open();
        // console.log(error);
      });
    }
  
    modal:any=[1];
    product_modal(m:any,ev){
      // console.log(ev);
      if(ev.target.checked){
        this.modal.push(m);
      // console.log(m);
      // console.log(this.modal);
      }
      else {
        this.del(m);
      }
      return this.modal;
    }
    del(obj:string){
      const index: number = this.modal.indexOf(obj);
      if (index !== -1) {
        this.modal.splice(index, 1);
    }  
      // console.log(this.modal);
      
  }
    // select_all(modal){
  
    //     console.log("array",modal);
    //    for(var i of modal){
    //      isChecked=true
         
    //    }
        
        
  
    // }
    selected:boolean=false;
    ok=false;
    select_All(sub,m) {
      console.log("from selectall",sub);
      if(this.SelectedSubCategory==sub){
        for(let p of m){
          console.log(p);
          this.modal.push(p);
          this.selected=!this.selected;
         //  this.ok=!this.ok;
         
         //  this.okb();
         //  return this.selected;
        }
      }
     for(let p of m){
       console.log(p);
      //  this.modal.push(p);
      //  this.selected=!this.selected;
      //  this.ok=!this.ok;
      
      //  this.okb();
      //  return this.selected;
     }
      
    }
    okb(){
      this.ok=!this.ok;
    }
    selected_cat_subcat(cat,sub){
      // alert('hii');
      this.SelectedSubCategory=sub;
    }
    selected_modals(cat,sub){
      
      let b=cat;
      let c=btoa(sub);
    let d= btoa(this.modal);
    let e= btoa("All")
    
     
       this.router.navigate(['/category', b,c,d,e]);
  //     if(this.modal==''){
  //       alert("select Product Modal No");
  //     }
  //     else {
  //       let b=btoa(cat);
  //       let c=btoa(sub);
      
      
  //      this.router.navigate(['/category', b,c]);
  // // console.log(c,"",s);
  // // this.catgeory_prod=c;
  // //     this.subcategory_prod=s;
  // //     this.select_prod="Selected";
  // //     this.modal;
  // //     return this.getData();
  //     }
    }
    sam(){
      // let navigationExtras: NavigationExtras = {
       
      // };
      let b=btoa("dam");
      
      
       this.router.navigate(['/category', b]);
    }
    selected1_modals(p,q){
      // console.log(p,"---",q);
      
      // console.log(this.modal);
  
      
    }
    //ok button Disabled
    ok_btn(){
// console.log("tttt",this.modal);

      if(this.modal==1){
        return true;
      }
      return false;
    }

    selected_catg(cat){
      let category=cat;
      this.router.navigate(['/prod-category',category]);
    }
    view_all_subcatg(category){
      this.router.navigate(['/prod-category',category]);
    }
   
}
