import { Component, OnInit } from '@angular/core';
import { GlobalServiceService } from '../global-service.service';
import { Router } from '@angular/router';
import { NgxSmartModalService } from 'ngx-smart-modal';
import { NgxSpinnerService } from 'ngx-spinner';

@Component({
  selector: 'app-product-menu2',
  templateUrl: './product-menu2.component.html',
  styleUrls: ['./product-menu2.component.scss']
})
export class ProductMenu2Component implements OnInit {
  resources: any;

  constructor( private service:GlobalServiceService,private router:Router, private ngxSmartService: NgxSmartModalService, private spinner: NgxSpinnerService) { }

  ngOnInit() {
    this.getprodlist();
  }
  getprodlist(){
    this.spinner.show();
    return this.service.getDatawithInput_id('14').subscribe((resp: Response) => {  
      this.spinner.hide();
      this.resources = resp; console.log(this.resources);
       },
       error => {
         this.ngxSmartService.getModal('errorModal').open();
         // console.log(error);
       });
  }
}
