import { Component, OnInit } from '@angular/core';
import { GlobalServiceService } from '../global-service.service';
import { NgxSpinnerService } from 'ngx-spinner';
import { NavigationExtras, Router } from '@angular/router';
import { NgxSmartModalService } from 'ngx-smart-modal';

@Component({
  selector: 'app-sample',
  templateUrl: './sample.component.html',
  styleUrls: ['./sample.component.scss']
})
export class SampleComponent implements OnInit {
  resources: any = [];
  resources1: any = [];
  resources2: any = [];
  // isChecked:boolean=false;
  constructor(private service: GlobalServiceService, private router: Router, private ngxSmartService: NgxSmartModalService, private spinner: NgxSpinnerService) { }
  catgeory_prod: any;
  subcategory_prod: any;
  select_prod: any;

  ngOnInit() {
    this.getprodimg();
    // this.getData1();
  }
  getprodimg() {
    this.spinner.show();
    return this.service.getDatawithInput_id('14').subscribe((resp: Response) => {
      this.spinner.hide();
      console.log(resp);

      this.resources = resp;
    },
      error => {
        this.ngxSmartService.getModal('errorModal').open();
        // console.log(error);
      });
  }

  product(a: any, r: any) {
    console.log(a, '', r);
    console.log(this.modal);

    this.catgeory_prod = a;
    this.subcategory_prod = r;
    this.select_prod = "All";
    this.modal;
    return this.getData();
  }
  getData() {
    this.spinner.show();
    return this.service.getDatawithQueryParams4('10', this.catgeory_prod, this.subcategory_prod, this.select_prod, this.modal).subscribe((resp: Response) => {
      this.spinner.hide();
      console.log(resp);

      this.resources1 = resp;
    },
    error => {
      this.ngxSmartService.getModal('errorModal').open();
      // console.log(error);
    });
  }

  modal: any = [];
  product_modal(m: any, ev) {
    console.log(ev);
    if (ev.target.checked) {
      this.modal.push(m);
      console.log(m);
      console.log(this.modal);
    }
    else {
      this.del(m);
    }
    return this.modal;
  }
  del(obj: string) {
    const index: number = this.modal.indexOf(obj);
    if (index !== -1) {
      this.modal.splice(index, 1);
    }
    console.log(this.modal);

  }
  // select_all(modal){

  //     console.log("array",modal);
  //    for(var i of modal){
  //      isChecked=true

  //    }



  // }
  selected_modals(cat, sub) {
    let b = btoa(cat);
    let c = btoa(sub);


    this.router.navigate(['/category', b, c]);
    //     if(this.modal==''){
    //       alert("select Product Modal No");
    //     }
    //     else {
    //       let b=btoa(cat);
    //       let c=btoa(sub);


    //      this.router.navigate(['/category', b,c]);
    // // console.log(c,"",s);
    // // this.catgeory_prod=c;
    // //     this.subcategory_prod=s;
    // //     this.select_prod="Selected";
    // //     this.modal;
    // //     return this.getData();
    //     }
  }
  sam() {
    // let navigationExtras: NavigationExtras = {

    // };
    let b = btoa("dam");


    this.router.navigate(['/category', b]);
  }
  selected1_modals(p, q) {
    console.log(p, "---", q);

    console.log(this.modal);


  }
}
