import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { GlobalServiceService } from '../global-service.service';
// import { Category } from "../category-list/category";
import { DataServiceService } from '../data-service.service';
import { Options } from 'ng5-slider';
import { NgxSmartModalService } from 'ngx-smart-modal';
import { NgxSpinnerService } from 'ngx-spinner';

@Component({
  selector: 'app-search-list',
  templateUrl: './search-list.component.html',
  styleUrls: ['./search-list.component.scss']
})
export class SearchListComponent implements OnInit {

  Page: any = 1;
  category: any;
  sub_c: any;
  sub: any;
  d: any;
  e: any;
  f: any;
  resources2: any;
  option: any;
  modal: any = [];
  range: any = [];
  select: any;
  order: string;
  catgHidden: boolean = false;
  discount: number = 1;
  selected_disc: number = 2;
  search_val: any;
  wish_color = "#a09898";
  p2: string;
  p3: string;
  p4: string;
  constructor(private spinner: NgxSpinnerService, private router: Router, private route: ActivatedRoute, private service: GlobalServiceService, public dataService: DataServiceService, private ngxSmartService: NgxSmartModalService) {

  }

  ngOnInit() {


    this.sub = this.route.params.subscribe(params => {
      this.search_val = params['search'];

      console.log("search", this.search_val);
      if (this.search_val === undefined) {
        this.d = params['b'];
        this.sub_c = params['c'];
        this.modal = atob(params['d']);
        this.select = atob(params['e']);
        // this.d=this.category;
        this.e = atob(this.sub_c);
        console.log(this.d, "", this.e, "", this.select, this.modal);
        this.getdata1();
      }
      else {
        this.getsearch();
      }

    },
      error => {
        this.ngxSmartService.getModal('errorModal').open();
        // console.log(error);
      });

  }
  getdata1() {
    this.spinner.show();
    return this.service.getDatawithQueryParams4('10', this.d, this.e, this.select, this.modal).subscribe((resp: Response) => {
      this.spinner.hide();
      console.log(resp);

      this.resources2 = resp;
    },
      error => {
        this.ngxSmartService.getModal('errorModal').open();
        // console.log(error);
      });

  }
  getsearch() {
    this.spinner.show();
    this.p2 = "";
    this.p3 = "All";
    return this.service.getDatawithQueryParams3('7.3', this.search_val, this.p2, this.p3).subscribe((resp: Response) => {
      this.spinner.hide();
      console.log(resp);


      this.resources2 = resp;
    },
      error => {
        this.ngxSmartService.getModal('errorModal').open();
        // console.log(error);
      });
  }

  //price Range
  minValue: number = 500;
  maxValue: number = 10000;
  options: Options = {
    floor: 500,
    ceil: 10000,
    step: 500
  };

  sub_cat(p) {
    console.log(p);
    // let category=btoa(p.category);
    // let sub_category=btoa(p.subcategory);
    // let model=btoa(p.modelno);
    let category = p.category;
    let sub_category = p.subcategory;
    let model = p.modelno;
    console.log("your Category", category, "hhh", sub_category, model);

    let obj = p;
    localStorage.setItem('key', JSON.stringify(obj));
    // console.log(p);
    // this.obj.setCategory(p);

    //  this.router.navigateByUrl('/product-detail');
    this.router.navigate(['/product-detail', category, sub_category, model]);
  }


  //price order-filter
  Ascend() {
    this.spinner.show();
    this.p4 = "";
    this.order = "";
    this.order = "acc";
    console.log(this.order);

    return this.service.getDatawithQueryParams7('7.3', this.search_val, this.p2, this.p3, this.p4, this.order, this.range, this.discount).subscribe((resp: Response) => {
      this.spinner.hide();
      console.log(resp);

      this.resources2 = resp; console.log(this.resources2);
    },
      error => {
        this.ngxSmartService.getModal('errorModal').open();
        // console.log(error);
      });

  }

  Descend() {
    this.spinner.show();
    this.order = "";
    this.order = "dec";
    return this.service.getDatawithQueryParams7('7.3', this.search_val, this.p2, this.p3, this.p4, this.order, this.range, this.discount).subscribe((resp: Response) => {
      this.spinner.hide();
      console.log(resp);

      this.resources2 = resp; console.log(this.resources2);
    },
      error => {
        this.ngxSmartService.getModal('errorModal').open();
        // console.log(error);
      });
  }


  //price-filter
  price_min(min, m) {
    this.spinner.show();
    this.range = [];
    // console.log(min);
    // console.log(m);
    let mini: number = +min;
    let maxi: number = +m
    this.range.push(mini);
    this.range.push(maxi);
    console.log("range-", this.range);
    let order = "";
    return this.service.getDatawithQueryParams7('7.3', this.search_val, this.p2, this.p3, this.p4, this.order, this.range, this.discount).subscribe((resp: Response) => {
      this.spinner.hide();
      console.log(resp);

      this.resources2 = resp; console.log(this.resources2);
    },
      error => {
        this.ngxSmartService.getModal('errorModal').open();
        // console.log(error);
      });
  }

  price_max(min, m) {
    console.log(min);
    console.log(m);
  }

  price(min, max) {
    console.log(min, "--", max);

  }
  disp_catg() {
    this.catgHidden = !this.catgHidden;
  }

  disc(p) {
    this.spinner.show();
    console.log(p);
    this.selected_disc = p;
    this.discount = p;
    return this.service.getDatawithQueryParams7('7.3', this.search_val, this.p2, this.p3, this.p4, this.order, this.range, this.discount).subscribe((resp: Response) => {
      this.spinner.hide();
      console.log(resp);

      this.resources2 = resp; console.log(this.resources2);
    },
      error => {
        this.ngxSmartService.getModal('errorModal').open();
        // console.log(error);
      });

  }

  //wishlist-code
  wish_list() {
    alert('hii');

  }
}
