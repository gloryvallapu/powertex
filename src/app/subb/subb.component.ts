import { Component, OnInit } from '@angular/core';
import * as $ from "jquery";
import { GlobalServiceService } from '../global-service.service';
import { NgxSmartModalService } from 'ngx-smart-modal';
import { NgxSpinnerService } from 'ngx-spinner';

@Component({
  selector: 'app-subb',
  templateUrl: './subb.component.html',
  styleUrls: ['./subb.component.scss']
})
export class SubbComponent implements OnInit {
  resources: any;

  constructor(private service: GlobalServiceService, private ngxSmartService: NgxSmartModalService, private spinner: NgxSpinnerService) { }

  ngOnInit() {
    this.getprodimg();
    $('li.parent').on('mouseover', function () {
      var $menuItem = $(this),
        $submenuWrapper = $('> .wrapper', $menuItem);

      // grab the menu item's position relative to its positioned parent
      var menuItemPos = $menuItem.position();

      // place the submenu in the correct position relevant to the menu item
      $submenuWrapper.css({
        top: menuItemPos.top,
        left: menuItemPos.left + Math.round($menuItem.outerWidth() * 0.75)
      });
    });
  }
  getprodimg() {
    this.spinner.show();
    return this.service.getDatawithInput_id('14').subscribe((resp: Response) => {
      this.spinner.hide();
      this.resources = resp;
    },
      error => {
        this.ngxSmartService.getModal('errorModal').open();
        // console.log(error);
      });
  }
}