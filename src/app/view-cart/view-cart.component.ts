import { Component, OnInit, ViewChild } from '@angular/core';
import { GlobalServiceService } from '../global-service.service';
import { DataServiceService } from '../data-service.service';
import { Router } from '@angular/router';
import { ComponentCommunicationService } from "../component-communication.service";
import { NgxSpinnerService } from 'ngx-spinner';
import { NgxSmartModalService } from 'ngx-smart-modal';

declare var $: any;

@Component({
  selector: 'app-view-cart',
  templateUrl: './view-cart.component.html',
  styleUrls: ['./view-cart.component.scss']
})
export class ViewCartComponent implements OnInit {
  input_id: string;
  user_id: string;
  count: number = 0;
  qty: number;
  carItems: any = [];
  setPosition: any;
  location: Coordinates;
  methodname: string;
  body: { "user_id": string; "productid": string; "qty": number; };
  quantity: number;
  CartItem: any = {};
  grandtotal: any;
  prodId: any;
  loginUserData: any;
  removeItem: any;
  obj: any = {};
  message: any;
  wish_alert: string;
  icon: boolean;
  alert: boolean;

  constructor(private globalService: GlobalServiceService, private route: Router,private ngxSmartService: NgxSmartModalService, private spinner: NgxSpinnerService, 
    private dataservice: DataServiceService, private eventEmmit: ComponentCommunicationService) {
    this.loginUserData = JSON.parse(localStorage.getItem('loginUserData'));
    console.log(this.loginUserData);
    this.obj.id = 1;
  }

  ngOnInit() {
    this.viewcart();
  }

  viewcart() {
    this.spinner.show();
    this.input_id = "4.2";
    this.user_id = this.loginUserData.user_id;
    this.globalService.getDatawithQueryParams1(this.input_id, this.user_id).subscribe((data) => {
      this.spinner.hide();
      if (data.status == "success") {
        this.carItems = data.values;
        console.log(data.values);
        this.obj.cartItem_count = data.values.length;
        this.eventEmmit.fire(this.obj);
      }
      console.log(this.carItems);
      this.grandtotal = 0;
      this.carItems.forEach(data => {
        this.grandtotal = this.grandtotal + ((data.total));
      });
    },
      error => {
        this.ngxSmartService.getModal('errorModal').open();
        // console.log(error);
      });
  }

  increment(data) {
    data.qty++;
    this.updateCart(data);
  }

  decrement(data) {
    if (data.qty > 1) {
      data.qty--;
      this.updateCart(data);
    }

  }

  updateCart(data) {
    this.spinner.show();
    this.methodname = "addtocart_site/";
    this.body = { "user_id": this.loginUserData.user_id, "productid": data.productid, "qty": data.qty }
    this.globalService.postData(this.body, this.methodname).subscribe((data) => {
      this.spinner.hide();
      console.log(data);
      if (data.Status == "Update sucessfully") {
        // this.message = data.Status;
        this.wish_alert = 'Updated sucessfully';
        this.addwish();
        // this.icon = true;
        //alert("cart Updated Successfully");
        //$('#cartUpdatedModal').modal('show');
        this.viewcart();
      }
    },
    error => {
      this.ngxSmartService.getModal('errorModal').open();
      // console.log(error);
    });
  }

  addwish() {
    this.alert = true;
    setInterval(() => {
      this.alert = false;
    }, 5000);
  }

  remove(item) {
    this.removeItem = item;
    $('#confirmModal').modal('show');
  }

  deleteConfirm() {
    this.spinner.show();
    $('#confirmModal').modal('hide');
    this.user_id = this.loginUserData.user_id;
    this.methodname = 'delete_cart/?user_id=' + this.user_id + '&productid=' + this.removeItem;
    this.globalService.deleteData(this.methodname).subscribe((data) => {
      this.spinner.hide();
      this.viewcart();
    },
    error => {
      this.ngxSmartService.getModal('errorModal').open();
      // console.log(error);
    });
  }

  gotoCheckout() {
    this.route.navigateByUrl('checkout/1');
  }

}
