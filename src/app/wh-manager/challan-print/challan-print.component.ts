import { Component, OnInit } from '@angular/core';
import { GlobalServiceService } from '../../global-service.service';
import { NgxSpinnerService } from 'ngx-spinner';

@Component({
  selector: 'app-challan-print',
  templateUrl: './challan-print.component.html',
  styleUrls: ['./challan-print.component.scss']
})
export class ChallanPrintComponent implements OnInit {
  challanData: any;
  challanPrint: any;
  billing_seq_no: any;
  shipping_seq_no: any;
  billingAddress: any;
  shipingAddress: any;
  challantableData: any;

  constructor(private globalService: GlobalServiceService, private spinner: NgxSpinnerService) {
    this.challanData = JSON.parse(localStorage.getItem('challanPrint'));
    this.challantableData = JSON.parse(localStorage.getItem('challanListData'));
    this.challanPrint = this.challanData.data.json_hdr;
    console.log(this.challanPrint);

    this.billing_seq_no = this.challanPrint.bill_to_party_seq_no;
    this.shipping_seq_no = this.challanPrint.ship_to_party_seq_no;
  }

  ngOnInit() {
    this.challan_printAddress();
  }

  challan_printAddress() {
    this.spinner.show();
    let input_id = "7.8";
    let param1 = this.billing_seq_no + "," + this.shipping_seq_no;
    console.log(param1);
    return this.globalService.getDatawithQueryParams1(input_id, param1).subscribe(data => {
      this.spinner.hide();
      console.log(data);
      this.billingAddress = data.Bill;
      this.shipingAddress = data.SHIP;
    });
  }


}
