import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { GetChallanListComponent } from './get-challan-list.component';

describe('GetChallanListComponent', () => {
  let component: GetChallanListComponent;
  let fixture: ComponentFixture<GetChallanListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ GetChallanListComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(GetChallanListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
