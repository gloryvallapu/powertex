import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { GlobalServiceService } from '../../global-service.service';
import { NgxSmartModalService } from 'ngx-smart-modal';
import { NgxSpinnerService } from 'ngx-spinner';

@Component({
  selector: 'app-get-challan-list',
  templateUrl: './get-challan-list.component.html',
  styleUrls: ['./get-challan-list.component.scss']
})
export class GetChallanListComponent implements OnInit {
  searchText: any;
  challanList: any = [];
  p: any = 1;
  loginUserData: any;

  constructor(private route: Router, private globalService: GlobalServiceService, private ngxSmartService: NgxSmartModalService, private spinner: NgxSpinnerService) { }

  ngOnInit() {
    this.loginUserData = JSON.parse(localStorage.getItem('loginUserData'));
    console.log(this.loginUserData);
    if(this.loginUserData===null){
      this.route.navigateByUrl('home');
    }
    this.getChallanList();
  }
  getChallanList() {
    this.spinner.show();
    this.globalService.getDatawithQueryParams1nd4(3.9, 22, this.loginUserData.company_code).subscribe((data) => {
      this.spinner.hide();
      this.challanList = data;
    },
      error => {
        this.ngxSmartService.getModal('errorModal').open();
        // console.log(error);
      });
  };
  makeInvoice(data) {
    localStorage.setItem('challanData', JSON.stringify(data));
    this.route.navigateByUrl('challan-generation');
  }
}
