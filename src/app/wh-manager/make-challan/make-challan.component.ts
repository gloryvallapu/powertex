import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { GlobalServiceService } from '../../global-service.service';
import { formatDate } from "@angular/common";
import { NgxSmartModalService } from 'ngx-smart-modal';
import { NgxSpinnerService } from 'ngx-spinner';
declare var $: any;


@Component({
  selector: 'app-make-challan', 
  templateUrl: './make-challan.component.html',
  styleUrls: ['./make-challan.component.scss']
})
export class MakeChallanComponent implements OnInit {
  p: any = 1;
  loginUserData: any;
  challanData: any;
  challanListData: any;
  challanDataModal: any = {};
  todayDate: any = new Date();
  challanPrint: any;
  stockDataList: any;

  constructor(private route: Router, private globalService: GlobalServiceService, private ngxSmartService: NgxSmartModalService, private spinner: NgxSpinnerService) { }

  ngOnInit() {
    this.loginUserData = JSON.parse(localStorage.getItem('loginUserData'));
    if(this.loginUserData===null){
      this.route.navigateByUrl('home');
    }
    this.challanData = JSON.parse(localStorage.getItem('challanData'));
    console.log(this.challanData);
    this.challanDataModal.del_mode = 'Vehicle';

    this.getChallanData();
  };

  getChallanData() {
    this.spinner.show();
    this.globalService.getDatawithQueryParams5(3.9, 23, this.challanData.inv_no, this.challanData.inv_date, this.loginUserData.company_code, this.challanData.financial_year).subscribe((data) => {
      this.spinner.hide();
      this.challanListData = data;
      localStorage.setItem('challanListData', JSON.stringify(data));
    },
      error => {
        this.ngxSmartService.getModal('errorModal').open();
        // console.log(error);
      });
  };


  generateChallan(form) {
    this.spinner.show();
    this.challanData.contact_num = this.challanDataModal.contact_num;
    this.challanData.del_consin_no = this.challanDataModal.del_consin_no;
    this.challanData.del_driver_name = this.challanDataModal.del_driver_name;
    this.challanData.del_license_no = this.challanDataModal.del_license_no;
    this.challanData.del_mode = this.challanDataModal.del_mode;
    this.challanData.del_vehicle_no = this.challanDataModal.del_vehicle_no;
    this.challanData.del_challan_date = formatDate(this.todayDate, 'yyyyMMdd', 'en-US');
    console.log(this.challanData);
    let body = {
      "process_in": 'CHALLAN', "operation_in": "UPDATE", "draft_final_in": "FINAL", "document_no_out": "", "message_out": "",
      "json_hdr": this.challanData, "json_dtl": ""
    }
    let methodName = "insert_update/"
    this.globalService.postData(body, methodName).subscribe((data) => {
      this.spinner.hide();
      this.challanPrint = data.data;
      console.log(this.challanPrint);
      localStorage.setItem('challanPrint', JSON.stringify(data));
      if (data.status = "challan generated") {
        $('#succModal').modal('show');
      }
    },
      error => {
        this.ngxSmartService.getModal('errorModal').open();
        // console.log(error);
      });
  };

  gotoChallanPrint() {
    this.route.navigateByUrl('challan_print');
  }
  gotoPrevious() {
    this.route.navigateByUrl('challan-list');
  };

  stockUpdate() {
    this.challanListData.forEach(data => {
      let stockData = {
        "productid": data.productid,
        "qty_in": data.inv_qty,
        "tot_amt_in": data.tot_value
      }
      this.stockDataList.push(stockData);
    });
    let body = {
      "period_in": formatDate(this.todayDate, 'yyyyMM', 'en-US'),
      "company_code_in": this.loginUserData.company_code,
      "product_details": this.stockDataList,
      "storage_location_in": "WAREHOUSE1",
      "process_type_in": "Challan"
    };
    console.log(body);
    let methodName = "proc_stock/"
    this.globalService.postData(body, methodName).subscribe((data) => {
      console.log(data);
    },
      error => {
        this.ngxSmartService.getModal('errorModal').open();
        //console.log(error);
      });

  };

}
