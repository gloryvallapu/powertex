import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { GlobalServiceService } from '../../global-service.service';
import { formatDate } from "@angular/common";
import { NgxSmartModalService } from 'ngx-smart-modal';
import { NgxSpinnerService } from 'ngx-spinner';
declare var $: any;

@Component({
  selector: 'app-make-packing-list',
  templateUrl: './make-packing-list.component.html',
  styleUrls: ['./make-packing-list.component.scss']
})
export class MakePackingListComponent implements OnInit {
  poData: any;
  packingListModel: any = {};
  poPackingListData: any;
  p: any = 1;
  todayDate = new Date();
  headerData: any;
  packingList: any = [];
  keys: string[];
  loginUserData: any;
  message: any;
  packingListNo: any;
  stockDataList: any = [];
  approveBtn: any = false;
  index: any;
  packingListprint: any;

  constructor(private route: Router, private globalService: GlobalServiceService, private ngxSmartService: NgxSmartModalService, private spinner: NgxSpinnerService) {

  }

  ngOnInit() {
    this.poData = JSON.parse(localStorage.getItem('poData'));
    this.loginUserData = JSON.parse(localStorage.getItem('loginUserData'));
    if(this.loginUserData===null){
      this.route.navigateByUrl('home');
    }
    console.log(this.poData);

    this.headerData = {
      document_no: 1,
      document_date: formatDate(this.todayDate, 'yyyyMMdd', 'en-US'),
      dest_company_code: this.poData.po_origin_company_code,
      source_company_code: this.poData.company_code,
      inv_type: 'DOM',
      shipment_point: this.poData.shipment_point,
      payment_terms: this.poData.payment_terms,
      currency_code: this.poData.currency_code,
      exchange_rate: this.poData.exchange_rate,
      spl_instr: this.poData.spl_instr,
      ship_to_party_seq_no: this.poData.ship_to_party_seq_no,
      bill_to_party_seq_no: this.poData.bill_to_party_seq_no,
      credit_period: this.poData.credit_period,
      created_user_id: this.loginUserData.user_id,
      remarks1: this.poData.remarks1,
      remarks2: this.poData.remarks1
    };
    this.getpoData();
  };

  getpoData() {
    this.spinner.show();
    this.globalService.getDatawithQueryParams4(3.9, 5, this.poData.po_no, this.poData.po_date, this.poData.po_origin_company_code).subscribe((data) => {
      this.spinner.hide();
      this.poPackingListData = data;
      this.poPackingListData.forEach(element => {
        this.packingListModel[element.po_srl_no] = element.balance_qty;
      });
    },
      error => {
        this.ngxSmartService.getModal('errorModal').open();
        // console.log(error);
      });
  };

  checkQty(qty, packQty) {
    if (packQty > qty) {
      this.approveBtn = true;
    } else {
      this.approveBtn = false;
    }
  }
  makePackingLiat() {
    this.spinner.show();
    this.approveBtn = false;
    this.packingList = [];
    this.keys = Object.keys(this.packingListModel);
    for (let i = 0; i < this.poPackingListData.length; i++) {
      for (let j = 0; j < this.keys.length; j++) {
        if (this.poPackingListData[i].po_srl_no == this.keys[j]) {
          if (this.packingListModel[this.keys[j]] <= this.poPackingListData[i].qty) {

            let json_dtl = {
              "document_no": i,
              "document_date": formatDate(this.todayDate, 'yyyyMMdd', 'en-US'),
              "po_no": this.poPackingListData[i].po_no,
              "po_date": (this.poPackingListData[i].po_date).replace(/-/g, ""),
              "discount_eff": this.poPackingListData[i].discount_eff,
              "performainv_date": formatDate(this.todayDate, 'yyyyMMdd', 'en-US'),
              "po_srl_no": this.poPackingListData[i].po_srl_no,
              "productid": this.poPackingListData[i].productid,
              "packing_qty": this.packingListModel[this.keys[j]],
              "net_price": this.poPackingListData[i].net_price,
              "performa_inv_qty": 0,
              "mrp": this.poPackingListData[i].mrp,
              "tot_value": JSON.parse(this.packingListModel[this.keys[j]]) * JSON.parse(this.poPackingListData[i].net_price),
              "srl_no": i + 1,
              "inv_srl_no": 0,
              "inv_qty": 0
            }
            this.packingList.push(json_dtl);
          } else {
            this.packingList = [];
            this.approveBtn = true;
            break;
          }
        }
      }
    }

    if (!this.approveBtn) {
      
      let body = {
        "process_in": 'PACKING', "operation_in": "INSERT", "draft_final_in": "FINAL", "document_no_out": "", "message_out": "",
        "json_dtl": this.packingList, "json_hdr": this.headerData
      }
      let methodName = "insert_update/"
      this.globalService.postData(body, methodName).subscribe((data) => {
        this.spinner.hide();
        this.packingListprint = data;
        localStorage.setItem('packingListprint', JSON.stringify(data));
        if (data.Message == "Packing list Sucessfully inserted ") {
          this.message = data.Message;
          this.packingListNo = data.Packing_list;
          $('#makeinvoiceModal').modal('show');
          //  this.stockUpdate();
        }
      },
        error => {
          this.ngxSmartService.getModal('errorModal').open();
          // console.log(error);
        });
    }
  };

  paking_list_print_page() {
    this.route.navigateByUrl('packing_list_print');
  };


}
