import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { GlobalServiceService } from '../../global-service.service';
import { NgxSmartModalService } from 'ngx-smart-modal';
import { NgxSpinnerService } from 'ngx-spinner';

@Component({
  selector: 'app-packing-list',
  templateUrl: './packing-list.component.html',
  styleUrls: ['./packing-list.component.scss']
})
export class PackingListComponent implements OnInit {
  poList: any = [];
  p: any = 1;
  loginUserData: any;
  searchText: any;
  constructor(private route: Router, private globalService: GlobalServiceService, private ngxSmartService: NgxSmartModalService, private spinner: NgxSpinnerService) { }

  ngOnInit() {

    // window.addEventListener("beforeunload", function (e) {
    //   console.log(e);

    // });


    this.loginUserData = JSON.parse(localStorage.getItem('loginUserData'));
    if(this.loginUserData===null){
      console.log("status111",this.loginUserData);
      this.route.navigateByUrl('home');
    }
    // console.log("status",this.loginUserData);
    this.getPOList();
  }
  getPOList() {
    this.spinner.show();
    this.globalService.getDatawithQueryParams1nd4(3.9, 1, this.loginUserData.company_code).subscribe((data) => {
      this.spinner.hide();
      this.poList = data;
    },
    error => {
      this.ngxSmartService.getModal('errorModal').open();
      // console.log(error);
    });
  };

  makePackingList(data) {
    localStorage.setItem('poData', JSON.stringify(data));
    // window.open('http://localhost:4200/packing-List');
    this.route.navigateByUrl('packing-List');
  };

  close() {
    //  window.close();
    // window.addEventListener("beforeunload", function (e) {
    //   console.log(e);

    // });
  }
}
